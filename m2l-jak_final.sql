-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 16 Juin 2015 à 15:36
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `m2l-jak`
--

-- --------------------------------------------------------

--
-- Structure de la table `basketarbitre`
--

CREATE TABLE IF NOT EXISTS `basketarbitre` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nomArbitre` varchar(30) NOT NULL,
  `prenomArbitre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `basketarbitre`
--

INSERT INTO `basketarbitre` (`id`, `nomArbitre`, `prenomArbitre`) VALUES
(1, 'Bele', 'Mathieu'),
(2, 'Cadill', 'Jérôme'),
(3, 'Cochin', 'Elise'),
(4, 'Dupont', 'Cécile'),
(5, 'Labard', 'Valérie');

-- --------------------------------------------------------

--
-- Structure de la table `basketcateg`
--

CREATE TABLE IF NOT EXISTS `basketcateg` (
  `bcaId` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`bcaId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `basketcateg`
--

INSERT INTO `basketcateg` (`bcaId`, `libelle`) VALUES
(1, 'Poussin'),
(2, 'Junior'),
(3, 'Senior');

-- --------------------------------------------------------

--
-- Structure de la table `basketchampionnat`
--

CREATE TABLE IF NOT EXISTS `basketchampionnat` (
  `bcId` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `annee` varchar(4) DEFAULT NULL,
  `idCateg` int(11) DEFAULT NULL,
  PRIMARY KEY (`bcId`),
  KEY `idCateg` (`idCateg`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `basketchampionnat`
--

INSERT INTO `basketchampionnat` (`bcId`, `nom`, `annee`, `idCateg`) VALUES
(1, 'ChampP', '2014', 1),
(2, 'ChampJ', '2014', 2),
(3, 'ChampS', '2014', 3);

-- --------------------------------------------------------

--
-- Structure de la table `basketequipe`
--

CREATE TABLE IF NOT EXISTS `basketequipe` (
  `beId` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `nbPoint` int(11) DEFAULT NULL,
  `idChamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`beId`),
  KEY `idChamp` (`idChamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `basketequipe`
--

INSERT INTO `basketequipe` (`beId`, `nom`, `logo`, `nbPoint`, `idChamp`) VALUES
(1, 'BourgPoussin', '', 6, 1),
(2, 'AmberieuPoussin', '', 0, 1),
(3, 'BelleyPoussin', '', 4, 1),
(4, 'CeyzeriatPoussin', '', 7, 1),
(5, 'BourgSenior', '', 0, 3),
(6, 'OyonnaxSenior', '', 0, 3),
(7, 'BelleySenior', '', 0, 3),
(8, 'CeyzeriatSenior', '', 0, 3),
(9, 'NantuaSenior', '', 0, 3),
(10, 'TrevouxSenior', '', 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `basketjourneematch`
--

CREATE TABLE IF NOT EXISTS `basketjourneematch` (
  `idChamp` int(11) NOT NULL DEFAULT '0',
  `nJournee` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idChamp`,`nJournee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `basketjourneematch`
--

INSERT INTO `basketjourneematch` (`idChamp`, `nJournee`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9),
(3, 10);

-- --------------------------------------------------------

--
-- Structure de la table `basketmatch`
--

CREATE TABLE IF NOT EXISTS `basketmatch` (
  `idChamp` int(11) NOT NULL DEFAULT '0',
  `idJournee` int(11) NOT NULL DEFAULT '0',
  `bmId` int(11) NOT NULL DEFAULT '0',
  `dateMatch` date DEFAULT NULL,
  `Score1` int(4) DEFAULT NULL,
  `Score2` int(4) DEFAULT NULL,
  `Equipe1` int(11) DEFAULT NULL,
  `Equipe2` int(11) DEFAULT NULL,
  `etatMatch` tinyint(1) NOT NULL,
  `idArbitre` int(5) DEFAULT NULL,
  PRIMARY KEY (`idJournee`,`idChamp`,`bmId`),
  KEY `Equipe1` (`Equipe1`),
  KEY `Equipe2` (`Equipe2`),
  KEY `idArbitre` (`idArbitre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `basketmatch`
--

INSERT INTO `basketmatch` (`idChamp`, `idJournee`, `bmId`, `dateMatch`, `Score1`, `Score2`, `Equipe1`, `Equipe2`, `etatMatch`, `idArbitre`) VALUES
(1, 1, 1, '2014-10-01', 34, 20, 1, 2, 0, NULL),
(1, 1, 2, '2014-10-01', 21, 21, 3, 4, 0, NULL),
(3, 1, 1, '2014-12-24', 56, 75, 5, 9, 0, NULL),
(3, 1, 2, '2014-12-24', 42, 43, 6, 10, 0, NULL),
(3, 1, 3, '2014-12-24', -1, -1, 7, 8, 0, NULL),
(1, 2, 1, '2014-10-25', 13, 15, 1, 3, 1, 2),
(1, 2, 2, '2014-10-25', 15, 21, 2, 4, 0, NULL),
(3, 2, 1, '2014-12-24', 102, 52, 5, 6, 0, NULL),
(3, 2, 2, '2014-12-24', 76, 54, 10, 8, 0, NULL),
(3, 2, 3, '2014-12-24', 65, 67, 7, 9, 0, NULL),
(1, 3, 1, '2014-11-03', 25, 18, 4, 1, 1, 1),
(1, 3, 2, '2014-11-03', 41, 23, 3, 2, 1, 2),
(3, 3, 1, '2015-05-18', 14, 14, 5, 7, 1, 3),
(3, 3, 2, '2015-05-02', 14, 15, 6, 8, 0, NULL),
(3, 3, 3, '2015-05-07', 14, 17, 9, 10, 0, NULL),
(1, 4, 1, '2014-01-10', 13, 15, 2, 1, 0, NULL),
(1, 4, 2, '2014-01-10', 14, 13, 4, 3, 1, 1),
(1, 5, 1, '2014-02-04', 13, 15, 3, 1, 1, 2),
(1, 5, 2, '2014-02-04', 84, 76, 4, 2, 1, 3),
(1, 6, 1, '2014-03-16', 93, 86, 1, 4, 1, 4),
(1, 6, 2, '2014-03-16', 74, 84, 2, 3, 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `tenniscategorie`
--

CREATE TABLE IF NOT EXISTS `tenniscategorie` (
  `tcaId` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`tcaId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `tenniscategorie`
--

INSERT INTO `tenniscategorie` (`tcaId`, `libelle`) VALUES
(1, 'Poussin'),
(2, 'Junior'),
(3, 'Senior');

-- --------------------------------------------------------

--
-- Structure de la table `tennisjoueur`
--

CREATE TABLE IF NOT EXISTS `tennisjoueur` (
  `jId` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `dateNaiss` date DEFAULT NULL,
  `sexe` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`jId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `tennisjoueur`
--

INSERT INTO `tennisjoueur` (`jId`, `nom`, `prenom`, `dateNaiss`, `sexe`) VALUES
(1, 'Dupont', 'Alain', '1985-10-28', 'H'),
(2, 'Frurito', 'Bernaud', '1981-02-28', 'H'),
(3, 'Durand', 'Garde', '1979-08-10', 'H'),
(4, 'Ducant', 'Finiard', '1994-08-12', 'H'),
(5, 'Pepe', 'Kuntz', '1995-06-05', 'H'),
(6, 'Baissourt', 'Trichot', '1996-10-28', 'H'),
(7, 'Dixon', 'Daric', '1998-02-25', 'H'),
(8, 'Deporte', 'coin', '1999-06-11', 'H'),
(9, 'Snow', 'Roberta', '1966-08-23', 'F'),
(10, 'Gentil', 'Bourru', '1993-04-15', 'F'),
(11, 'Renault', 'Pauget', '1950-05-10', 'F'),
(12, 'young', 'Evieux', '1969-06-05', 'F');

-- --------------------------------------------------------

--
-- Structure de la table `tennismatch`
--

CREATE TABLE IF NOT EXISTS `tennismatch` (
  `tmId` int(11) NOT NULL AUTO_INCREMENT,
  `demiFinale` tinyint(1) DEFAULT NULL,
  `Tournois` int(11) NOT NULL,
  `etatMatch` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmId`),
  KEY `tournois` (`Tournois`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `tennismatch`
--

INSERT INTO `tennismatch` (`tmId`, `demiFinale`, `Tournois`, `etatMatch`) VALUES
(1, 0, 1, 1),
(2, 0, 1, 1),
(3, 1, 1, 1),
(4, 0, 2, 0),
(5, 0, 2, 0),
(6, 1, 2, 1),
(7, 0, 3, 1),
(8, 0, 3, 1),
(9, 1, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tennisparticipe`
--

CREATE TABLE IF NOT EXISTS `tennisparticipe` (
  `idMatch` int(11) NOT NULL DEFAULT '0',
  `idJoueur` int(11) NOT NULL DEFAULT '0',
  `scoreSet1` int(11) DEFAULT NULL,
  `scoreSet2` int(11) DEFAULT NULL,
  `scoreSet3` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMatch`,`idJoueur`),
  KEY `idJoueur` (`idJoueur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tennisparticipe`
--

INSERT INTO `tennisparticipe` (`idMatch`, `idJoueur`, `scoreSet1`, `scoreSet2`, `scoreSet3`) VALUES
(1, 1, 0, 30, 30),
(1, 2, 45, 45, 45),
(2, 3, 6, 7, 7),
(2, 4, 4, 5, 6),
(3, 1, 6, 6, 1),
(3, 3, 3, 7, 6),
(4, 11, NULL, NULL, NULL),
(4, 12, NULL, NULL, NULL),
(5, 9, NULL, NULL, NULL),
(5, 10, NULL, NULL, NULL),
(6, 9, 3, 7, 4),
(6, 12, 6, 6, 6),
(7, 5, 13, 14, 0),
(7, 6, 17, 23, 0),
(8, 7, 13, 14, 0),
(8, 8, 17, 23, 0),
(9, 5, 13, 14, 21),
(9, 7, 17, 23, 25);

-- --------------------------------------------------------

--
-- Structure de la table `tennistournois`
--

CREATE TABLE IF NOT EXISTS `tennistournois` (
  `ttId` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `genre` varchar(1) DEFAULT NULL,
  `categorie` int(11) NOT NULL,
  PRIMARY KEY (`ttId`),
  KEY `categorie` (`categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `tennistournois`
--

INSERT INTO `tennistournois` (`ttId`, `nom`, `date`, `genre`, `categorie`) VALUES
(1, 'Bourg TCB', '2014-10-20', 'H', 3),
(2, 'Nantua', '2014-10-20', 'F', 3),
(3, 'Trevoux', '2014-10-20', 'H', 2),
(4, 'Chatillon', '2014-10-25', 'H', 3),
(5, 'Macon', '2014-10-25', 'H', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `sport` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `mdp`, `sport`) VALUES
(1, 'utBasket', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'basket'),
(2, 'utTennis', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'tennis'),
(3, 'lpaul', 'b4sk37', 'basket');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `basketchampionnat`
--
ALTER TABLE `basketchampionnat`
  ADD CONSTRAINT `basketchampionnat_ibfk_1` FOREIGN KEY (`idCateg`) REFERENCES `basketcateg` (`bcaId`);

--
-- Contraintes pour la table `basketequipe`
--
ALTER TABLE `basketequipe`
  ADD CONSTRAINT `basketequipe_ibfk_1` FOREIGN KEY (`idChamp`) REFERENCES `basketchampionnat` (`bcId`);

--
-- Contraintes pour la table `basketjourneematch`
--
ALTER TABLE `basketjourneematch`
  ADD CONSTRAINT `basketjourneematch_ibfk_1` FOREIGN KEY (`idChamp`) REFERENCES `basketchampionnat` (`bcId`);

--
-- Contraintes pour la table `basketmatch`
--
ALTER TABLE `basketmatch`
  ADD CONSTRAINT `basketmatch_ibfk_3` FOREIGN KEY (`idArbitre`) REFERENCES `basketarbitre` (`id`),
  ADD CONSTRAINT `basketmatch_ibfk_1` FOREIGN KEY (`Equipe1`) REFERENCES `basketequipe` (`beId`),
  ADD CONSTRAINT `basketmatch_ibfk_2` FOREIGN KEY (`Equipe2`) REFERENCES `basketequipe` (`beId`);

--
-- Contraintes pour la table `tennismatch`
--
ALTER TABLE `tennismatch`
  ADD CONSTRAINT `tournois` FOREIGN KEY (`Tournois`) REFERENCES `tennistournois` (`ttId`);

--
-- Contraintes pour la table `tennisparticipe`
--
ALTER TABLE `tennisparticipe`
  ADD CONSTRAINT `idMatch` FOREIGN KEY (`idMatch`) REFERENCES `tennismatch` (`tmId`),
  ADD CONSTRAINT `tennisparticipe_ibfk_1` FOREIGN KEY (`idMatch`) REFERENCES `tennismatch` (`tmId`),
  ADD CONSTRAINT `tennisparticipe_ibfk_2` FOREIGN KEY (`idJoueur`) REFERENCES `tennisjoueur` (`jId`);

--
-- Contraintes pour la table `tennistournois`
--
ALTER TABLE `tennistournois`
  ADD CONSTRAINT `categorie` FOREIGN KEY (`categorie`) REFERENCES `tenniscategorie` (`tcaId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
