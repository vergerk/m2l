Dépôt GIT pour le projet "Maison des Ligues de Lorraine" du Lycée Carriat (Bourg-en-Bresse) - 2015
Projet présenté lors des examens pratiques et validé.

Les "Maisons des Ligues de Lorraine" est un Projet Professionnel de l'Etudiant, réalisé en cours et en groupe.
Deux PPE ont eu lieu sur ce thème :

- application Web, pour la consultation des résultats

- application cliente, pour la mise à jour des données. Ce dépôt traite de cette partie.

Ce PPE a été traité en suivant [l'architecture trois-tiers](https://fr.wikipedia.org/wiki/Architecture_trois_tiers) :

- un client, exploité par les utilisateurs

- un serveur qui reçoit les demandes clients et réalise le traitement avec la base de données.

- la base de données, déjà existante (conçu lors du précédent PPE).

J'ai donc conçu la partie "serveur", basé sur les Sockets pour la transmission. Des bibliothèques ont été réalisé afin de répondre aux besoins communs entre le serveur et le client.

Un prototype de client a été également conçu à des fins de débogage du serveur. Il ne reflète en rien l'interface définitive destinée aux utilisateurs.

Plus d'informations sur www.vergerk.fr/projets#m2l