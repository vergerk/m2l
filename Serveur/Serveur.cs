﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// ajouts
using System.Net;
using System.Net.Sockets;
using BiblioComm;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace Serveur
{
    public partial class serv : Form
    {
        // longueur max d'un message
        private static int lgMessage = 60000; 

        // adresses IP et ports
        private IPAddress adrIpLocale;
        private IPAddress adrIpBdD;
        private static int portServeur = 33000;

        private static int portClient = 33001;

        // classe pour définir le type d'IP, le datagramme et le protocole
        private Socket sockReception;

        // point de terminaison (IP + port)
        private IPEndPoint epRecepteur;

        // message, en binaire
        byte[] messageBytes;

        // connexion à la BdD
        private Sql connexion;

        // liste des connectés
        Dictionary<string, string> listeConnectes;

        /*
         *  Cat         liste des catégories
         *  J           liste des journées
         *  M           liste des matchs
         *  Aj          ajoute les scores
         *  Connexion   connexion de l'utilisateur
         *  Resultat    résultat de la demande
         *  Deconnexion
         */

        public serv()
        {
            InitializeComponent();
            
            // gestion du masque de saisie
            txt_ipBdD.Mask = @"###\.###\.###\.###";
            txt_ipBdD.ValidatingType = typeof(System.Net.IPAddress);

            // récupère la liste des adresses IP du poste et les insères dans la ComboBox
            foreach (string ip in UtilIP.GetAdrIpLocaleV4())
            {
                cb_listeInterfaces.Items.Add(ip);
            }

            if (cb_listeInterfaces.Items.Count == 0)
            {
                MessageBox.Show("Aucune interface détectée.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                // s'il y a qu'une interface et que l'IP serveur est saisie, connexion immédiate
                if (
                    (cb_listeInterfaces.Items.Count == 1) &&
                    (txt_ipBdD.Text != "   .   .   .")
                )
                {
                    ConnexionServeur();
                }

                cb_listeInterfaces.SelectedIndex = 0;

                // écoute
                adrIpLocale = IPAddress.Parse(cb_listeInterfaces.SelectedItem.ToString());
                initReception();
            }
        }

        #region envoi/réception

        /// <summary>
        /// Ecoute et attend la réception de données
        /// </summary>
        private void initReception()
        {
            // instanciation d'un dictionnaire
            listeConnectes = new Dictionary<string, string>();

            // on stocke les données reçus, d'une longueur max LGMESSAGE, dans une variable de la classe
            messageBytes = new byte[lgMessage];

            // 1) instanciation + définition de la transmission : IPv4, Datagrammes, UDP
            sockReception = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            // 2) création d'un point de terminaison émetteur, avec l'adresse IP locale + port à occuper
            epRecepteur = new IPEndPoint(adrIpLocale, portServeur);

            // 3) liaison
            sockReception.Bind(epRecepteur);

            // 4) création du point de terminaison récepteur. Aucune IP ni de port puisqu'inconnus
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

            // on indique que le serveur a démarré, avec l'IP
            AjoutEtat("Le serveur a démarré en utilisant l'IP:port " + Convert.ToString(adrIpLocale) + ":" + portServeur, "information");

            // 5) socket en attente de réception
            sockReception.BeginReceiveFrom(messageBytes, 0, lgMessage, SocketFlags.None, ref epTemp, new AsyncCallback(recevoir), null);
        }

        /// <summary>
        /// Réception des données
        /// </summary>
        /// <param name="AR"></param>
        private void recevoir(IAsyncResult AR)
        {
            // création du point de terminaison récepteur. Aucune IP ni de port puisqu'inconnus
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

            // met fin à la lecture des données asynchrones
            //sockReception.EndReceiveFrom(donnees_asynchrones, point_de_terminaison);
            sockReception.EndReceiveFrom(AR, ref epTemp);

            // désérialisation du message reçu
            Comm leMessage = Binaire.Deserialiser(messageBytes);

            // préparation en vue de traiter les données en parallèle 
            // (thread différent de celui du formulaire = utilisation possible du chat tout en attendant la réception)
            BackgroundWorker worker = new BackgroundWorker();

            // délégué
            /* 
                * Lorsque vous créez un délégué DoWorkEventHandler, vous identifiez la méthode qui gérera l'événement. 
                * Pour associer l'événement à votre gestionnaire d'événements, ajoutez une instance du délégué à l'événement. 
                * La méthode du gestionnaire d'événements est appelée chaque fois que l'événement se produit, 
                * sauf si vous supprimez le délégué. 
                * 
                * http://msdn.microsoft.com/fr-fr/library/17sde2xt%28v=vs.110%29.aspx
            */
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);

            // Se produit lorsque l'opération d'arrière-plan est terminée, a été annulée ou a levé une exception.
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            // Démarre l'exécution d'une opération d'arrière-plan.
            worker.RunWorkerAsync(leMessage);

            // Définit une plage des éléments d'un tableau avec la valeur par défaut de chaque type d'élément.
            // Array.Clear effacera le contenu du tableau à partir d'un index, pour x éléments
            // Array.Clear(tableau, debut_index, nombres_elements_a_effacer);
            Array.Clear(messageBytes, 0, messageBytes.Length);

            // en attente de réception
            sockReception.BeginReceiveFrom(messageBytes, 0, lgMessage, SocketFlags.None, ref epTemp, new AsyncCallback(recevoir), null);
        }

        /// <summary>
        /// Instanciation d'un nouveau délégué
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = e.Argument;
        }

        /// <summary>
        /// Envoi du message au client
        /// </summary>
        /// <param name="leMessage"></param>
        /// <param name="destinataire"></param>
        private void envoyerUnicast(Comm leMessage, IPAddress destinataire)
        {
            try
            {
                byte[] messageMulticast;

                // création du socket et point de terminaison et liaison
                Socket sockEmission = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint epEmetteur = new IPEndPoint(adrIpLocale, 0);
                sockEmission.Bind(epEmetteur);

                // destinataire du message (IP + port)
                IPAddress IpMulticast = destinataire;
                IPEndPoint epRecepteur = new IPEndPoint(IpMulticast, portClient);

                // transforme en binaire
                messageMulticast = Binaire.Serialiser(leMessage);

                // envoi + fermeture du socket
                sockEmission.SendTo(messageMulticast, epRecepteur);
                sockEmission.Close();
            }
            catch (Exception e)
            {
                AjoutEtat(e.Message, "important");
            }
        }

        #endregion

        /// <summary>
        /// Traitement du message reçu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // l'évènement est un objet de type MessageReseau
            Comm leMessage = (Comm)e.Result;

            object objetSport = leMessage.ObjetSport;

            // cas où le serveur est actif mais non-connecté à la BdD
            if (connexion != null)
            {
                // on vérifie que l'utilisateur est déjà connecté ...
                if (
                    listeConnectes.ContainsKey(leMessage.CleEmetteur) && 
                    leMessage.TypeMessage != "Deconnexion"
                    )
                {
                    string cleEmetteur = leMessage.CleEmetteur;

                    #region Basket

                    if (listeConnectes[leMessage.CleEmetteur] == "Basket")
                    {
                        Basket sportBasket = null;

                        if (leMessage.TypeMessage != "Deconnexion")
                        {
                            sportBasket = (Basket)objetSport;
                        }

                        switch (leMessage.TypeMessage)
                        {
                            case "Cat":
                                // liste des catégories
                                DataSet categories = connexion.getDataSet(
                                    "SELECT DISTINCT bcaId AS Id, libelle as libelle " +
                                    "FROM (((basketcateg AS bCat INNER JOIN basketchampionnat AS bChamp ON bCat.bcaId = bChamp.idCateg) " +
                                        "INNER JOIN basketequipe AS bEq ON bChamp.bcId = bEq.IdChamp) " +
                                        "INNER JOIN basketmatch AS bMat ON bEq.beId = bMat.Equipe1) " +
                                        "INNER JOIN basketequipe AS bEq2 ON bEq2.beId = bMat.Equipe2 " +
                                    "WHERE etatMatch = 0"
                                    );

                                AjoutEtat("Catégories de basket demandées", "consultation");

                                Comm messageCategories = new Comm(adrIpLocale, cleEmetteur, "Resultat", categories);
                                envoyerUnicast(messageCategories, leMessage.IpEmetteur);
                                break;

                            case "J":
                                // liste des journées
                                DataSet journees = connexion.getDataSet(
                                    "SELECT DISTINCT IdJournee " +
                                    "FROM (basketchampionnat AS bc INNER JOIN basketjourneematch AS bjm ON bjm.idChamp = bc.bcId) " +
                                    "INNER JOIN basketmatch as bm ON bm.idChamp = bc.bcId " +
                                    "WHERE bc.idCateg = " + sportBasket.Categorie + " AND etatMatch = 0"
                                    );

                                AjoutEtat("Journées demandées : catégorie " + sportBasket.Categorie, "consultation");

                                Comm messageJournees = new Comm(adrIpLocale, cleEmetteur, "Resultat", journees);
                                envoyerUnicast(messageJournees, leMessage.IpEmetteur);
                                break;

                            case "M":
                                // liste des matchs
                                DataSet matchs = connexion.getDataSet(
                                    "SELECT basketmatch.idChamp AS 'idChamp', idJournee, bmId, dateMatch, Score1, Score2, e1.nom AS 'E1', e2.nom AS 'E2', etatMatch, idArbitre " +
                                    "FROM " +
                                        "(basketmatch INNER JOIN basketequipe AS e1 ON e1.beId = basketmatch.Equipe1) " +
                                        "INNER JOIN basketequipe AS e2 ON e2.beId = basketmatch.Equipe2 " +
                                    "WHERE IdJournee = '" + sportBasket.Journee + "' AND basketmatch.idChamp = (" +
                                        "SELECT bcId " +
                                        "FROM basketchampionnat " +
                                        "WHERE idCateg = '" + sportBasket.Categorie + "' AND etatMatch = 0" +
                                        ")"
                                    );

                                AjoutEtat("Matchs demandés : catégorie " + sportBasket.Categorie + " / journée : " + sportBasket.Journee, "consultation");

                                Comm messageMatchs = new Comm(adrIpLocale, cleEmetteur, "Resultat", matchs);
                                envoyerUnicast(messageMatchs, leMessage.IpEmetteur);
                                break;

                            case "ArbitreBask":
                                // liste des arbitres
                                DataSet arbitres = connexion.getDataSet(
                                    "SELECT * FROM basketarbitre ORDER BY id, nomArbitre, prenomArbitre"
                                    );

                                AjoutEtat("Liste des arbitres demandées", "consultation");

                                Comm messageArbitres = new Comm(adrIpLocale, cleEmetteur, "Resultat", arbitres);
                                envoyerUnicast(messageArbitres, leMessage.IpEmetteur);
                                break;

                            case "Aj":

                                // on vérifie si le moment qu'on souhaite MàJ existe
                                DataSet basketControle = connexion.getDataSet(
                                    "SELECT etatMatch FROM basketmatch " +
                                    "WHERE IdJournee = '" + sportBasket.Journee + "' AND bmId = '" + sportBasket.Match + "' AND etatMatch = '0' AND IdChamp = (" +
                                        "SELECT bcId " +
                                        "FROM basketchampionnat " +
                                        "WHERE idCateg = '" + sportBasket.Categorie + "'" +
                                        ")"
                                    );
                                
                                // on regarde si la mise à jour a bien fonctionné. Si non, notification du client et infos dans l'historique serveur
                                if (basketControle.Tables[0].Rows.Count != 0)
                                {
                                    // modification d'un match + contrôle de la MàJ
                                    string requeteMaJ =
                                        "UPDATE basketmatch " +
                                        "SET " +
                                            "Score1 = '" + sportBasket.ScoreA + "', " +
                                            "Score2 = '" + sportBasket.ScoreB + "', " +
                                            "idArbitre = '" + sportBasket.Arbitre + "', " +
                                            "etatMatch = 1 " +
                                        "WHERE IdJournee = '" + sportBasket.Journee + "' AND bmId = '" + sportBasket.Match + "' AND IdChamp = (" +
                                            "SELECT bcId " +
                                            "FROM basketchampionnat " +
                                            "WHERE idCateg = '" + sportBasket.Categorie + "'" +
                                            ")";

                                    // exécution de la requête
                                    connexion.executRqt(requeteMaJ);

                                    DataSet basketMaJ = connexion.getDataSet(
                                        "SELECT etatMatch FROM basketmatch " +
                                        "WHERE IdJournee = '" + sportBasket.Journee + "' AND bmId = '" + sportBasket.Match + "' AND etatMatch = '1' AND IdChamp = (" +
                                            "SELECT bcId " +
                                            "FROM basketchampionnat " +
                                            "WHERE idCateg = '" + sportBasket.Categorie + "'" +
                                            ")"
                                        );

                                    // on vérifie que la mise à jour a bien fonctionné. Si non, notification du client et infos dans l'historique serveur
                                    if (basketMaJ.Tables[0].Rows.Count == 0)
                                    {
                                        Comm resultatMaJ = new Comm(adrIpLocale, cleEmetteur, "Erreur", true, "Erreur dans la mise à jour du match.");
                                        envoyerUnicast(resultatMaJ, leMessage.IpEmetteur);

                                        AjoutEtat(
                                            "\n-BASKET----------------------------------------------------------------------\n" +

                                            "Erreur dans la modification des scores. Consultez les informations ci-dessous :\n" +
                                            "Cat : " + sportBasket.Categorie + " / J : " + sportBasket.Journee + " / Mat : " + sportBasket.Match +
                                            "/ Sc1 : " + sportBasket.ScoreA + " / Sc2 : " + sportBasket.ScoreB +
                                            "/ idArbitre : " + sportBasket.Arbitre + "\n\n" +

                                            "Requête exécutée :\n" + requeteMaJ + "\n" +

                                            "-------------------------------------------------------------------------------",
                                            "important"
                                            );
                                    }
                                    else
                                    {
                                        Comm resultatMaJ = new Comm(adrIpLocale, cleEmetteur, "Resultat", true, "Le match a bien été mis à jour.");
                                        envoyerUnicast(resultatMaJ, leMessage.IpEmetteur);

                                        AjoutEtat("BASKET : Modification des scores - Cat : " + sportBasket.Categorie + " / J : " + sportBasket.Journee +
                                            " / Mat : " + sportBasket.Match + " / Sc1 : " + sportBasket.ScoreA + " / Sc2 : " + sportBasket.ScoreB +
                                            " / Arbitre : " + sportBasket.Arbitre,
                                            "maj"
                                            );
                                    }
                                }
                                else
                                {
                                    Comm resultatMaJ = new Comm(adrIpLocale, cleEmetteur, "Erreur", true, "Le match n'a pas été mis à jour, vous avez fourni des informations incorrectes.");
                                    envoyerUnicast(resultatMaJ, leMessage.IpEmetteur);

                                    AjoutEtat(
                                        "\n-BASKET----------------------------------------------------------------------\n" +

                                        "Erreur dans la modification des scores. Un ou plusieurs informations sont impossibles :\n" +
                                        "Cat : " + sportBasket.Categorie + " / J : " + sportBasket.Journee + " / Mat : " + sportBasket.Match +
                                        "/ Sc1 : " + sportBasket.ScoreA + " / Sc2 : " + sportBasket.ScoreB +
                                        "/ idArbitre : " + sportBasket.Arbitre + "\n" +

                                        "-------------------------------------------------------------------------------",
                                        "important"
                                        );
                                }
                                break;
                        }
                    }

                    #endregion

                    #region Tennis

                    else if (listeConnectes[leMessage.CleEmetteur] == "Tennis")
                    {
                        Tennis sportTennis = null;

                        if (leMessage.TypeMessage != "Deconnexion")
                        {
                            sportTennis = (Tennis)objetSport;
                        }

                        switch (leMessage.TypeMessage)
                        {
                            case "Cat":
                                // liste des catégories
                                DataSet categories = connexion.getDataSet(
                                    "SELECT DISTINCT tcaId AS Id, libelle as libelle " +
                                    "FROM ((tenniscategorie AS tCat INNER JOIN tennistournois AS tTour ON tCat.tcaId = tTour.categorie) " +
                                        "INNER JOIN tennismatch AS tMat ON tMat.Tournois = tTour.ttId) " +
                                        "INNER JOIN tennisparticipe AS tPart ON tPart.idMatch = tMat.tmId " +
                                    "WHERE etatMatch = 0"
                                    );

                                AjoutEtat("Catégories de tennis demandées", "consultation");
                                
                                Comm messageCategories = new Comm(adrIpLocale, cleEmetteur, "Resultat", categories);
                                envoyerUnicast(messageCategories, leMessage.IpEmetteur);
                                break;

                            case "T":
                                // liste des tournois
                                DataSet tournois = connexion.getDataSet(
                                    "SELECT DISTINCT ttId " +
                                    "FROM (tennistournois AS tt INNER JOIN tennismatch AS tm ON tm.Tournois = tt.ttId) " +
                                    "INNER JOIN tennisparticipe AS tp ON tp.idMatch = tm.tmId " +
                                    "WHERE tt.categorie = '" + sportTennis.Categorie + "' AND etatMatch = 0"
                                    );

                                AjoutEtat("Journées demandées : catégorie " + sportTennis.Categorie, "consultation");

                                Comm messageJournees = new Comm(adrIpLocale, cleEmetteur, "Resultat", tournois);
                                envoyerUnicast(messageJournees, leMessage.IpEmetteur);
                                break;

                            case "M":
                                // liste des matchs
                                DataSet matchs = connexion.getDataSet(
                                    "SELECT idMatch, idJoueur, tJou.nom AS 'nom', scoreSet1, ScoreSet2, ScoreSet3, etatMatch " +
                                    "FROM ((tennistournois AS tt INNER JOIN tennismatch AS tm ON tm.Tournois = tt.ttId) " +
                                    "INNER JOIN tennisparticipe AS tp ON tp.idMatch = tm.tmId) " +
                                    "INNER JOIN tennisjoueur AS tJou ON tJou.jId = tp.idJoueur " +
                                    "WHERE tt.categorie = '" + sportTennis.Categorie + "' AND etatMatch = 0 AND Tournois = '" + sportTennis.Tournoi + "'"
                                    );

                                AjoutEtat("Matchs demandés : catégorie " + sportTennis.Categorie + " / journée : " + sportTennis.Tournoi, "consultation");

                                Comm messageMatchs = new Comm(adrIpLocale, cleEmetteur, "Resultat", matchs);
                                envoyerUnicast(messageMatchs, leMessage.IpEmetteur);
                                break;

                            case "Aj":
                                // modification d'un match + contrôle de la MàJ
                                string sc3_J1;
                                string sc3_J2;

                                // cas où l'utilisateur a saisi le 3e set
                                if ((sportTennis.Set3J1 != null) && (sportTennis.Set3J2 != null))
                                {
                                    sc3_J1 = "'" + Convert.ToString(sportTennis.Set3J1) + "'";
                                    sc3_J2 = "'" + Convert.ToString(sportTennis.Set3J2) + "'";
                                }
                                else
                                {
                                    sc3_J1 = "NULL";
                                    sc3_J2 = "NULL";
                                }

                                // on vérifie que les données communiquées correspondent à des éléments existants                                
                                DataSet tennisControle = connexion.getDataSet(
                                    "SELECT idMatch, etatMatch, j1.jId AS idJoueur, j1.nom AS nomJoueur, tp.scoreSet1 AS Set1, tp.scoreSet2 AS Set2, tp.scoreSet3 AS Set3 " +
                                    "FROM " +
                                        "(tennismatch INNER JOIN tennisparticipe AS tp ON tennismatch.tmId = tp.idMatch) " +
                                        "INNER JOIN tennisjoueur AS j1 ON j1.jId = tp.idJoueur " +
                                    "WHERE idMatch = '" + sportTennis.Match + "' AND (idJoueur = '" + sportTennis.Joueur1 + "' OR idJoueur = '" + sportTennis.Joueur2 + "') " +
                                        "AND (scoreSet1 IS NULL OR scoreSet2 IS NULL OR etatMatch = 0) " +
                                    "ORDER BY idMatch, idJoueur, nomJoueur, Set1, Set2, Set3"
                                    );

                                if (tennisControle.Tables[0].Rows.Count != 0)
                                {
                                    string requeteMajJ1 =
                                        "UPDATE tennisparticipe " +
                                        "SET " +
                                            "scoreSet1 = '" + sportTennis.Set1J1 + "', " +
                                            "scoreSet2 = '" + sportTennis.Set2J1 + "', " +
                                            "scoreSet3 = " + sc3_J1 + " " +
                                        "WHERE idMatch = '" + sportTennis.Match + "' AND idJoueur = '" + sportTennis.Joueur1 + "'";

                                    string requeteMajJ2 =
                                        "UPDATE tennisparticipe " +
                                        "SET " +
                                            "scoreSet1 = '" + sportTennis.Set1J2 + "', " +
                                            "scoreSet2 = '" + sportTennis.Set2J2 + "', " +
                                            "scoreSet3 = " + sc3_J2 + " " +
                                        "WHERE idMatch = '" + sportTennis.Match + "' AND idJoueur = '" + sportTennis.Joueur2 + "'";

                                    string requeteMajMatch =
                                        "UPDATE tennismatch " +
                                        "SET etatMatch = 1 " +
                                        "WHERE tmId = '" + sportTennis.Match + "'";

                                    // exécution des requêtes
                                    connexion.executRqt(requeteMajJ1);
                                    connexion.executRqt(requeteMajJ2);
                                    connexion.executRqt(requeteMajMatch);

                                    // on vérifie que la mise à jour a bien fonctionné. Si non, notification du client et infos dans l'historique serveur
                                    string requeteControle =
                                        "SELECT idMatch, etatMatch, j1.jId AS idJoueur, j1.nom AS nomJoueur, tp.scoreSet1 AS Set1, tp.scoreSet2 AS Set2, tp.scoreSet3 AS Set3 " +
                                        "FROM " +
                                            "(tennismatch INNER JOIN tennisparticipe AS tp ON tennismatch.tmId = tp.idMatch) " +
                                            "INNER JOIN tennisjoueur AS j1 ON j1.jId = tp.idJoueur " +
                                        "WHERE idMatch = '" + sportTennis.Match + "' AND (idJoueur = '" + sportTennis.Joueur1 + "' OR idJoueur = '" + sportTennis.Joueur2 + "') " +
                                            "AND (scoreSet1 IS NULL OR scoreSet2 IS NULL OR etatMatch = 0) " +
                                        "ORDER BY idMatch, idJoueur, nomJoueur, Set1, Set2, Set3";

                                    DataSet tennisMaJ = connexion.getDataSet(requeteControle);

                                    if (tennisMaJ.Tables[0].Rows.Count != 0)
                                    {
                                        Comm resultatMaJ = new Comm(adrIpLocale, cleEmetteur, "Erreur", true, "Erreur dans la mise à jour du match.");
                                        envoyerUnicast(resultatMaJ, leMessage.IpEmetteur);

                                        AjoutEtat(
                                            "/n-------------------------------------------------------------------------------\n" +

                                            "Erreur dans la modification des scores. Consultez les informations ci-dessous :\n" +
                                            "J1 : " + sportTennis.Joueur1 + " / Mat : " + sportTennis.Match + " / " +
                                            "Set1 : " + sportTennis.Set1J1 + " / Set2 : " + sportTennis.Set2J1 + " / Set3 : " + sportTennis.Set3J1 + "\n\n" +

                                            "J2 : " + sportTennis.Joueur2 + " / Mat : " + sportTennis.Match + " / " +
                                            "Set1 : " + sportTennis.Set1J2 + " / Set2 : " + sportTennis.Set2J2 + " / Set3 : " + sportTennis.Set3J2 + "\n\n" +

                                            "Requête de contrôle :\n" + requeteControle + "\n" +

                                            "-------------------------------------------------------------------------------",
                                            "important"
                                            );

                                    }
                                    else
                                    {
                                        Comm resultatMaJ = new Comm(adrIpLocale, cleEmetteur, "Resultat", true, "Le match a bien été mis à jour.");
                                        envoyerUnicast(resultatMaJ, leMessage.IpEmetteur);

                                        AjoutEtat("TENNIS : Modification des scores - J : " + sportTennis.Joueur1 + " / Mat : " + sportTennis.Match + " / " +
                                            "Set1 : " + sportTennis.Set1J1 + " / Set2 : " + sportTennis.Set2J1 + " / Set3 : " + sportTennis.Set3J1, "maj");
                                        AjoutEtat("TENNIS : Modification des scores - J : " + sportTennis.Joueur2 + " / Mat : " + sportTennis.Match + " / " +
                                            "Set1 : " + sportTennis.Set1J2 + " / Set2 : " + sportTennis.Set2J2 + " / Set3 : " + sportTennis.Set3J2, "maj");
                                    }
                                }
                                else
                                {
                                    Comm resultatMaJ = new Comm(adrIpLocale, cleEmetteur, "Erreur", true, "Le match n'a pas été mis à jour, vous avez fourni des informations incorrectes.");
                                    envoyerUnicast(resultatMaJ, leMessage.IpEmetteur);

                                    AjoutEtat(
                                        "\n-TENNIS----------------------------------------------------------------------\n" +

                                        "Erreur dans la modification des scores. Un ou plusieurs éléments sont impossibles :\n" +
                                        "J1 : " + sportTennis.Joueur1 + " / Mat : " + sportTennis.Match + " / " +
                                        "Set1 : " + sportTennis.Set1J1 + " / Set2 : " + sportTennis.Set2J1 + " / Set3 : " + sportTennis.Set3J1 + "\n\n" +

                                        "J2 : " + sportTennis.Joueur2 + " / Mat : " + sportTennis.Match + " / " +
                                        "Set1 : " + sportTennis.Set1J2 + " / Set2 : " + sportTennis.Set2J2 + " / Set3 : " + sportTennis.Set3J2 + "\n" +

                                        "-----------------------------------------------------------------------------",
                                        "important"
                                        );
                                }
                                break;
                        }
                    }

                    #endregion
                }
                // ... ou qu'il demande à se connecter ...
                else if (leMessage.TypeMessage == "Connexion")
                {
                    string Identifiant = leMessage.Identifiant;
                    string Mdp = leMessage.Mdp;
                    string IP = leMessage.IpEmetteur.ToString();

                    AjoutEtat("Tentative de connexion : " + Identifiant + " (" + IP + ")", "information");

                    // déclaration
                    DataSet resultat = null;

                    if (
                        (leMessage.Param == "Basket") ||
                        (leMessage.Param == "Tennis")
                        )
                    {
                        resultat = connexion.getDataSet("SELECT id, sport FROM utilisateur WHERE login = '" + Identifiant + "' AND mdp = SHA1('" + Mdp + "') AND sport = '" + leMessage.Param + "'");
                    }

                    // s'il y a aucun résultat ou que le message ne contient pas le sport
                    if ((resultat.Tables[0].Rows.Count == 0) || (resultat == null))
                    {
                        Comm messageConnex = new Comm(adrIpLocale, "-1", "Resultat", false, "null");
                        envoyerUnicast(messageConnex, leMessage.IpEmetteur);

                        AjoutEtat("Echec de la connexion de " + Identifiant, "information");
                    }
                    else
                    {
                        string cleUtilisateur = resultat.Tables[0].Rows[0].ItemArray[0].ToString();
                        string sport = resultat.Tables[0].Rows[0].ItemArray[1].ToString();

                        AjoutUtilisateur(cleUtilisateur, sport, Identifiant);

                        Comm messageConnex = new Comm(adrIpLocale, cleUtilisateur, "Resultat", true, sport);
                        envoyerUnicast(messageConnex, leMessage.IpEmetteur);
                    }
                }
                else if (leMessage.TypeMessage == "Deconnexion")
                {
                    string Identifiant = leMessage.Identifiant;
                    string Mdp = leMessage.Mdp;
                    string IP = leMessage.IpEmetteur.ToString();

                    // contrôle de la demande de déconnexion (requête légitime ?)
                    DataSet Connex = connexion.getDataSet("SELECT id, sport FROM utilisateur WHERE login = '" + Identifiant + "' AND mdp = SHA1('" + Mdp + "')");

                    // si aucun résultat, refus de la déconnexion
                    if (Connex.Tables[0].Rows.Count == 0)
                    {
                        Comm messageConnex = new Comm(adrIpLocale, "-1", "Erreur", false, "Demande de déconnexion refusée.");
                        envoyerUnicast(messageConnex, leMessage.IpEmetteur);
                        AjoutEtat("Demande de déconnexion refusée : " + Identifiant, "information");
                    }
                    else
                    {
                        string cleUtilisateur = Connex.Tables[0].Rows[0].ItemArray[0].ToString();
                        string sport = Connex.Tables[0].Rows[0].ItemArray[1].ToString();

                        if (listeConnectes.ContainsKey(leMessage.CleEmetteur))
                        {
                            listeConnectes.Remove(leMessage.CleEmetteur);
                        }

                        Comm messageConnex = new Comm(adrIpLocale, cleUtilisateur, "Resultat", true, "deconnexion");
                        envoyerUnicast(messageConnex, leMessage.IpEmetteur);
                        AjoutEtat(Identifiant + " s'est déconnecté.", "information");
                    }
                }
                // ... sinon message d'erreur
                else
                {
                    Comm messageConnex = new Comm(adrIpLocale, "-1", "Resultat", false, "null");
                    envoyerUnicast(messageConnex, leMessage.IpEmetteur);
                    AjoutEtat("Echec d'une requête depuis " + leMessage.IpEmetteur + ", l'utilisateur n'était pas connecté.", "information");
                }
            }
            else // aucune connexion à la base de données
            {
                Comm messageConnex = new Comm(adrIpLocale, "-1", "Resultat", false, "Indisponible, réessayez plus tard.");
                envoyerUnicast(messageConnex, leMessage.IpEmetteur);
                AjoutEtat("Echec d'une requête depuis " + leMessage.IpEmetteur + " : aucune connexion vers la base de données.", "information");
            }
        }

        /// <summary>
        /// Ajout du traitement dans la boite d'historique
        /// </summary>
        /// <param name="etat"></param>
        private void AjoutEtat(string etat, string type)
        {
            switch (type)
            {
                default:
                    rtb_etatServeur.SelectionColor = Color.Black;
                    break;

                case "important":
                    rtb_etatServeur.SelectionColor = Color.Red;
                    break;

                case "consultation":
                    rtb_etatServeur.SelectionColor = Color.Black;
                    break;

                case "maj":
                    rtb_etatServeur.SelectionColor = Color.Purple;
                    break;

                case "information":
                    rtb_etatServeur.SelectionColor = Color.Blue;
                    break;
            }

            rtb_etatServeur.AppendText(DateTime.Now.ToString("HH:mm:ss") + " : " + etat + "\n");
            
            // défilement auto
            // http://geekswithblogs.net/Waynerds/archive/2006/01/29/67506.aspx
            rtb_etatServeur.SelectionStart = rtb_etatServeur.TextLength;
            rtb_etatServeur.ScrollToCaret();
        }

        /// <summary>
        /// Connexions utilisateurs
        /// </summary>
        /// <param name="cleUtilisateur"></param>
        /// <param name="sport"></param>
        /// <param name="identifiant"></param>
        private void AjoutUtilisateur(string cleUtilisateur, string sport, string identifiant)
        {
            // on regarde si la clé existe déjà. Si non, on l'ajoute
            if (!listeConnectes.ContainsKey(cleUtilisateur))
            {
                listeConnectes.Add(cleUtilisateur, sport);
                AjoutEtat(identifiant + " (" + cleUtilisateur + " - " + sport + ") est connecté.", "information");
            }
            else
            {
                AjoutEtat(identifiant + " (" + cleUtilisateur + ") " + " vient de se reconnecter", "information");
            }
        }

        /// <summary>
        /// Déconnexions utilisateurs
        /// </summary>
        /// <param name="cleUtilisateur"></param>
        private void SupprUtilisateur(string cleUtilisateur)
        {
            if (listeConnectes.ContainsKey(cleUtilisateur))
            {
                listeConnectes.Remove(cleUtilisateur);
                cleUtilisateur = "-1";
            }
        }

        #region Connexion/déconnexion à la base de données

        /// <summary>
        /// Vérifie l'IP et connecte
        /// </summary>
        private void ConnexionServeur()
        {
            // supprime les espaces dans l'IP et regarde si elle est correcte
            if (IPAddress.TryParse((txt_ipBdD.Text).Replace(" ", ""), out adrIpBdD))
            {
                btn_deconnexion.Enabled = true;

                cb_listeInterfaces.Enabled = false;
                btn_connexion.Enabled = false;
                txt_ipBdD.Enabled = false;

                // création de la connexion
                // IP serveur / BdD / identifiant / mot de passe
                connexion = new Sql(adrIpBdD.ToString(), "m2l-jak", "m2l-jak", "852456m2P");
                TestConnexion();
                adrIpLocale = IPAddress.Parse(cb_listeInterfaces.SelectedItem.ToString());

                if (sockReception == null)
                {
                    initReception();
                }
                AjoutEtat("IP base de données : " + adrIpBdD, "information");
            }
            else
            {
                MessageBox.Show("Vous n'avez pas indiqué l'adresse IP du serveur.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Déconnecte de la base de données et arrête l'écoute
        /// </summary>
        private void DeconnexionServeur()
        {
            btn_deconnexion.Enabled = false;

            cb_listeInterfaces.Enabled = true;
            btn_connexion.Enabled = true;
            txt_ipBdD.Enabled = true;

            AjoutEtat("Déconnecté.", "information");
        }

        /// <summary>
        /// Test de la connexion
        /// </summary>
        private void TestConnexion()
        {
            string r = connexion.testConnexion();

            if (r != "")
            {
                AjoutEtat("Erreur lors de la connexion à la BdD : " + r, "important");
                DeconnexionServeur();
            }
            else
            {
                AjoutEtat("Test de connexion réussi avec succès." + r, "information");
            }
        }

        /// <summary>
        /// Envoie un message d'erreur à l'utilisateur et l'indique dans la console du serveur
        /// </summary>
        private void ErreurConnexionBdD(string cleUtilisateur, IPAddress ipDestination)
        {
            AjoutEtat("Erreur de connexion à la base de données.", "important");
            Comm erreur = new Comm(adrIpLocale, cleUtilisateur, "Erreur", true, "Une erreur s'est produite, la demande n'a pu être traitée.");
            envoyerUnicast(erreur , ipDestination);
        }

        private void btn_connexion_Click(object sender, EventArgs e)
        {
            ConnexionServeur();
        }

        private void btn_deconnexion_Click(object sender, EventArgs e)
        {
            DeconnexionServeur();
        }

        #endregion

        /// <summary>
        /// Déplace le curseur au prochain bloc de saisie IP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ipserv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Decimal)
            {
                txt_ipBdD.SelectionStart = UtilIP.CurseurBlocIP(txt_ipBdD.SelectionStart);
            }
        }
    }
}
