﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

// ajouts
using MySql.Data.MySqlClient;
using System.Data;

namespace Serveur
{
    class Sql
    {
        private String srv;
        private String db;
        private String host;
        private String pwd;
        private MySqlConnection connexion;

        #region constructeurs

        /// <summary>
        /// Constructeur complet
        /// </summary>
        /// <param name="unSrv">server</param>
        /// <param name="uneDb">db</param>
        /// <param name="unHost">host</param>
        /// <param name="unPwd">passWord</param>

        public Sql(String unSrv, String uneDb, String unHost, String unPwd)
        {
            srv = unSrv;
            db = uneDb;
            host = unHost;
            pwd = unPwd;

            string chaineCnx = "Database=" + db + ";Data Source=" + srv + ";User Id=" + host + ";Password=" + pwd;
            connexion = new MySqlConnection();
            connexion.ConnectionString = chaineCnx;
        }

        /// <summary>
        /// Constructeur, sans base de données
        /// </summary>
        /// <param name="unSrv">server</param>
        /// <param name="unHost">host</param>
        /// <param name="unPwd">password</param>

        public Sql(String unSrv, String unHost, String unPwd)
        {
            srv = unSrv;
            host = unHost;
            pwd = unPwd;

            string chaineCnx = "Data Source=" + srv + ";User Id=" + host + ";Password=" + pwd;
            connexion = new MySqlConnection();
            connexion.ConnectionString = chaineCnx;
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="aConnectionString">Chaine de connexion MySQL</param>

        public Sql(String aConnectionString)
        {
            connexion = new MySqlConnection();
            connexion.ConnectionString = aConnectionString;
        }

        #endregion

        #region Méthodes

        /// <summary>
        /// Méthode qui exécute une requête sans retour
        /// </summary>
        /// <param name="aQuery">query</param>

        public void executRqt(String aQuery)
        {  
            try
            {
                if (connexion.State == System.Data.ConnectionState.Closed)
                {
                    connexion.Open();
                }

                MySqlDataAdapter da = new MySqlDataAdapter();
                da.SelectCommand = new MySqlCommand(aQuery, connexion);
                da.SelectCommand.ExecuteNonQuery();
            }
            catch (Exception) { }

            finally
            {
                closeCnx();
            }
        }

        /// <summary>
        /// Retourne le contenu de la requête
        /// </summary>
        /// <param name="aQuery">query</param>
        /// <returns>result</returns>

        public DataSet getDataSet(String aQuery)
        {
            DataSet ds = new DataSet();

            try
            {
                if (connexion.State == System.Data.ConnectionState.Closed)
                {
                    connexion.Open();
                }

                MySqlDataAdapter da = new MySqlDataAdapter(aQuery, connexion);
                da.Fill(ds);
            }

            catch (Exception) {}

            finally
            {
                closeCnx();
                MySqlDataAdapter da = new MySqlDataAdapter();
            }
            
            return ds;
        }

        /// <summary>
        /// Fermeture de la connexion
        /// </summary>

        public void closeCnx()
        {
            if (connexion.State == System.Data.ConnectionState.Open)
            {
                connexion.Close();
            }
        }

        /// <summary>
        /// Teste la connexion au serveur
        /// </summary>
        /// <returns></returns>
        public string testConnexion()
        {
            string r = "";

            try
            {
                connexion.Open();
            }
            catch (Exception e)
            {
                r = e.Message;
            }

            return r;
        }

        #endregion
    }
}
