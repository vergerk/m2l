﻿namespace Serveur
{
    partial class serv
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(serv));
            this.rtb_etatServeur = new System.Windows.Forms.RichTextBox();
            this.btn_connexion = new System.Windows.Forms.Button();
            this.cb_listeInterfaces = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_deconnexion = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_ipBdD = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // rtb_etatServeur
            // 
            this.rtb_etatServeur.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtb_etatServeur.BackColor = System.Drawing.Color.White;
            this.rtb_etatServeur.DetectUrls = false;
            this.rtb_etatServeur.Location = new System.Drawing.Point(13, 61);
            this.rtb_etatServeur.Name = "rtb_etatServeur";
            this.rtb_etatServeur.ReadOnly = true;
            this.rtb_etatServeur.Size = new System.Drawing.Size(509, 589);
            this.rtb_etatServeur.TabIndex = 0;
            this.rtb_etatServeur.Text = "";
            // 
            // btn_connexion
            // 
            this.btn_connexion.Location = new System.Drawing.Point(349, 8);
            this.btn_connexion.Name = "btn_connexion";
            this.btn_connexion.Size = new System.Drawing.Size(75, 23);
            this.btn_connexion.TabIndex = 1;
            this.btn_connexion.Text = "Connexion";
            this.btn_connexion.UseVisualStyleBackColor = true;
            this.btn_connexion.Click += new System.EventHandler(this.btn_connexion_Click);
            // 
            // cb_listeInterfaces
            // 
            this.cb_listeInterfaces.FormattingEnabled = true;
            this.cb_listeInterfaces.Location = new System.Drawing.Point(158, 10);
            this.cb_listeInterfaces.Name = "cb_listeInterfaces";
            this.cb_listeInterfaces.Size = new System.Drawing.Size(121, 21);
            this.cb_listeInterfaces.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Interface de réception client";
            // 
            // btn_deconnexion
            // 
            this.btn_deconnexion.Enabled = false;
            this.btn_deconnexion.Location = new System.Drawing.Point(430, 8);
            this.btn_deconnexion.Name = "btn_deconnexion";
            this.btn_deconnexion.Size = new System.Drawing.Size(92, 23);
            this.btn_deconnexion.TabIndex = 4;
            this.btn_deconnexion.Text = "Déconnexion";
            this.btn_deconnexion.UseVisualStyleBackColor = true;
            this.btn_deconnexion.Click += new System.EventHandler(this.btn_deconnexion_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "IP base de données";
            // 
            // txt_ipBdD
            // 
            this.txt_ipBdD.Location = new System.Drawing.Point(158, 37);
            this.txt_ipBdD.Mask = "###\\.###\\.###\\.###";
            this.txt_ipBdD.Name = "txt_ipBdD";
            this.txt_ipBdD.RejectInputOnFirstFailure = true;
            this.txt_ipBdD.ResetOnPrompt = false;
            this.txt_ipBdD.Size = new System.Drawing.Size(121, 20);
            this.txt_ipBdD.TabIndex = 7;
            this.txt_ipBdD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ipserv_KeyDown);
            // 
            // serv
            // 
            this.AcceptButton = this.btn_connexion;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 662);
            this.Controls.Add(this.txt_ipBdD);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_deconnexion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_listeInterfaces);
            this.Controls.Add(this.btn_connexion);
            this.Controls.Add(this.rtb_etatServeur);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "serv";
            this.Text = "Serveur";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_etatServeur;
        private System.Windows.Forms.Button btn_connexion;
        private System.Windows.Forms.ComboBox cb_listeInterfaces;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_deconnexion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txt_ipBdD;


    }
}

