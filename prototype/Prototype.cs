﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// ajouts
using System.Net;
using BiblioComm;
using System.Net.Sockets;

namespace prototype
{
    public partial class Prototype : Form
    {
        // longueur max d'un message
        private static int lgMessage = 60000;

        // Adresses IP et ports
        private IPAddress adrIpLocale;
        private IPAddress adrIpServeur;

        private static int portServeur = 33000;
        private static int portClient = 33001;

        // classe pour définir le type d'IP, le datagramme et le protocole
        private Socket sockReception;

        // point de terminaison (IP + port)
        private IPEndPoint epRecepteur;

        // message, en binaire
        byte[] messageBytes;

        // fin de la connexion
        bool finConnexion = false;

        // clé utilisateur
        string cleUtilisateur = "-1";

        // liste des éléments à activer/désactiver
        List<Control> ctrlBasket = new List<Control>();
        List<Control> ctrlTennis = new List<Control>();

        // objets pour transmettre les données au serveur
        private Basket sportBasket = new Basket();
        private Tennis sportTennis = new Tennis();

        // remplace le timeout du Socket (non géré en asynchrone)
        private Timer socketTimeOut = null;

        public Prototype()
        {
            InitializeComponent();

            lbl_idUtilisateur.Text = null;

            #region Liste pour bascule du "Enabled" des contrôles
            ctrlBasket.AddRange(new Control[] {
                // connexion et déconnexion
                btn_connexionBasket, 
                btn_deconnexionBasket,

                // interaction avec le serveur
                btn_catBasket,
                btn_listeArbitres,

                // champs pour l'interaction
                txt_bmIdBasket,
                txt_catBasket,
                txt_journBasket,
                txt_scABasket,
                txt_scBBasket,
                txt_idArbitre,

                // utilisateur
                txt_idUtilisateurBasket,
                txt_mdpBasket,
                lbl_idUtilisateur,

                // interface et communication
                tab_tennis,
                txt_ipServ,
                cb_listeInterfaces
            });

            ctrlTennis.AddRange(new Control[] {
                // connexion et déconnexion
                btn_connexionTennis,
                btn_deconnexionTennis,

                // interaction avec le serveur
                btn_catTennis,

                // champs pour l'interaction
                txt_catTennis,
                txt_catTennis2,
                txt_idMatchTennis,
                txt_tournoiTennis,
                
                txt_idJoueur1Tennis,
                txt_sc1_j1Tennis,
                txt_sc2_j1Tennis,
                txt_sc3_j1Tennis,

                txt_idJoueur2Tennis,
                txt_sc1_j2Tennis,
                txt_sc2_j2Tennis,
                txt_sc3_j2Tennis,

                // utilisateur
                txt_idUtilisateurTennis,
                txt_mdpTennis,
                lbl_idUtilisateur,

                // interface et communication
                tab_basket,
                txt_ipServ,
                cb_listeInterfaces
                });
            #endregion

            // gestion du masque de saisie
            txt_ipServ.Mask = @"###\.###\.###\.###";
            txt_ipServ.ValidatingType = typeof(System.Net.IPAddress);

            // récupère la liste des adresses IP du poste et les insères dans la ComboBox
            foreach (string ip in UtilIP.GetAdrIpLocaleV4())
            {
                cb_listeInterfaces.Items.Add(ip);
            }

            if (cb_listeInterfaces.Items.Count == 0)
            {
                MessageBox.Show("Aucune interface détectée.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                cb_listeInterfaces.SelectedIndex = 0;
            }
        }

        /*
        *  Cat         liste des catégories
        *  J           liste des journées
        *  M           liste des matchs
        *  Aj          ajoute les scores
        *  Connexion   connexion de l'utilisateur
        *  Resultat    résultat de la demande
        *  Erreur      messages d'erreurs provenant du serveur
        *  Deconnexion
        *  
        *  --- EXAMEN BTS E4 -----------------------------
        *  ArbitreBask   liste des arbitres pour le basket
        */

        #region envoi/réception

        /// <summary>
        /// Ecoute des ports
        /// </summary>
        private void initReception()
        {
            // on stocke les données reçus, d'une longueur max LGMESSAGE, dans une variable de la classe
            messageBytes = new byte[lgMessage];

            // 1) instanciation + définition de la transmission : IPv4, Datagrammes, UDP
            sockReception = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            // 2) création d'un point de terminaison émetteur, avec l'adresse IP locale + port à occuper
            epRecepteur = new IPEndPoint(adrIpLocale, portClient);

            // 3) liaison - n'a pas lieu si une connexion existe déjà
            sockReception.Bind(epRecepteur);

            // 4) création du point de terminaison récepteur. Aucune IP ni de port puisqu'inconnus
            EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

            // 5) socket en attente de réception
            sockReception.BeginReceiveFrom(messageBytes, 0, lgMessage, SocketFlags.None, ref epTemp, new AsyncCallback(recevoir), null);

            // Remplace "sockReception.ReceiveTimeout"
            NouveauTimer(10000);
        }

        /// <summary>
        /// Traitement des données reçus
        /// </summary>
        /// <param name="AR"></param>
        private void recevoir(IAsyncResult AR)
        {
            if (finConnexion)
            {
                // Demande de déconnexion
                finConnexion = false;
            }
            else
            {
                // création du point de terminaison récepteur. Aucune IP ni de port puisqu'inconnus
                EndPoint epTemp = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

                // met fin à la lecture des données asynchrones
                // sockReception.EndReceiveFrom(donnees_asynchrones, point_de_terminaison);
                sockReception.EndReceiveFrom(AR, ref epTemp);

                // désérialisation du message reçu
                Comm leMessage = Binaire.Deserialiser(messageBytes);

                // préparation en vue de traiter les données en parallèle 
                // (thread différent de celui du formulaire = utilisation possible du chat tout en attendant la réception)
                BackgroundWorker worker = new BackgroundWorker();

                // délégué
                /* 
                 * Lorsque vous créez un délégué DoWorkEventHandler, vous identifiez la méthode qui gérera l'événement. 
                 * Pour associer l'événement à votre gestionnaire d'événements, ajoutez une instance du délégué à l'événement. 
                 * La méthode du gestionnaire d'événements est appelée chaque fois que l'événement se produit, 
                 * sauf si vous supprimez le délégué. 
                 * 
                 * http://msdn.microsoft.com/fr-fr/library/17sde2xt%28v=vs.110%29.aspx
                */
                worker.DoWork += new DoWorkEventHandler(worker_DoWork);

                // Se produit lorsque l'opération d'arrière-plan est terminée, a été annulée ou a levé une exception.
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

                // Démarre l'exécution d'une opération d'arrière-plan.
                worker.RunWorkerAsync(leMessage);

                // Définit une plage des éléments d'un tableau avec la valeur par défaut de chaque type d'élément.
                // Array.Clear effacera le contenu du tableau à partir d'un index, pour x éléments
                // Array.Clear(tableau, debut_index, nombres_elements_a_effacer);
                Array.Clear(messageBytes, 0, messageBytes.Length);

                // en attente de réception
                sockReception.BeginReceiveFrom(messageBytes, 0, lgMessage, SocketFlags.None, ref epTemp, new AsyncCallback(recevoir), null);
            }
        }

        /// <summary>
        /// Instanciation d'un nouveau délégué
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = e.Argument;
        }

        /// <summary>
        /// Envoi du message au serveur
        /// </summary>
        /// <param name="leMessage"></param>
        /// <param name="destinataire"></param>
        private void envoyerUnicast(Comm leMessage, IPAddress destinataire)
        {
            try
            {
                byte[] messageMulticast;

                // création du socket et point de terminaison et liaison
                Socket sockEmission = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint epEmetteur = new IPEndPoint(adrIpLocale, 0);
                sockEmission.Bind(epEmetteur);

                // destinataire du message (IP + port)
                IPAddress IpMulticast = destinataire;
                IPEndPoint epRecepteur = new IPEndPoint(IpMulticast, portServeur);

                // transforme en binaire
                messageMulticast = Binaire.Serialiser(leMessage);

                // envoi + fermeture du socket
                sockEmission.SendTo(messageMulticast, epRecepteur);
                sockEmission.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Traitement du message reçu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Arrêt du chrono pour la fin de connexion
            ArretTimer();

            // l'évènement est un objet de type MessageReseau
            Comm resultats = (Comm)e.Result;
            rtb_resultats.Clear();

            if (resultats.Resultat == null) // le message n'est pas le résultat d'une requête SQL (information serveur)
            {
                if (resultats.InfoServeur == true)
                {
                    cleUtilisateur = resultats.CleEmetteur;
                    lbl_idUtilisateur.Text = resultats.CleEmetteur;

                    // on permet la modification du formulaire selon le sport renvoyé
                    if (resultats.Param == "Basket")
                    {
                        GestionSport(ref ctrlBasket);
                        btn_connexionBasket.Enabled = false;
                        AjoutEtat("Connexion réussie.");
                    }
                    else if (resultats.Param == "Tennis")
                    {
                        GestionSport(ref ctrlTennis);
                        btn_connexionTennis.Enabled = false;
                        AjoutEtat("Connexion réussie.");
                    }
                    else if (resultats.Param == "deconnexion") // cas de la déconnexion
                    {
                        DecoServeur();

                        AjoutEtat("Déconnecté du serveur.");
                    }
                    else if (resultats.TypeMessage == "Erreur")
                    {
                        AjoutEtat(resultats.Param);
                    }
                    else // pour toute autre information
                    {
                        AjoutEtat(resultats.Param);
                    }
                }
                else // connexion refusée
                {
                    if (resultats.Param == "")
                    {
                        AjoutEtat("Connexion utilisateur refusée ou manquante.");
                    }
                    else if (resultats.Param.Contains("Indisponible"))
                    {
                        AjoutEtat(resultats.Param);
                        DecoServeur();
                    }
                    else
                    {
                        AjoutEtat(resultats.Param);
                    }

                    cleUtilisateur = resultats.CleEmetteur;
                    lbl_idUtilisateur.Text = resultats.CleEmetteur;

                    DemandeDecoServ();
                }
            }
            else // affichage du résultat de la requête SQL
            {
                AjoutEtat(resultats.Resultat.GetXml()); ;
            }
        }

        #endregion

        #region Interaction avec le serveur

        #region Basket

        /// <summary>
        /// Liste des catégories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_catBasket_Click(object sender, EventArgs e)
        {
            Comm message = new Comm(adrIpLocale, cleUtilisateur, "Cat", "Basket");
            envoyerUnicast(message, adrIpServeur);
        }

        /// <summary>
        /// Liste des journées
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_journBasket_Click(object sender, EventArgs e)
        {
            sportBasket.AjCategories(Convert.ToInt32(txt_catBasket.Text));

            Comm message = new Comm(adrIpLocale, cleUtilisateur, "J", sportBasket, "Basket");
            envoyerUnicast(message, adrIpServeur);
        }

        /// <summary>
        /// Liste des matchs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_matchsBasket_Click(object sender, EventArgs e)
        {
            sportBasket.AjJournees(Convert.ToInt32(txt_journBasket.Text));

            Comm message = new Comm(adrIpLocale, cleUtilisateur, "M", sportBasket, "Basket");
            envoyerUnicast(message, adrIpServeur);
        }

        /// <summary>
        /// Liste des arbitres
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_listeArbitres_Click(object sender, EventArgs e)
        {
            Comm message = new Comm(adrIpLocale, cleUtilisateur, "ArbitreBask", "Basket");
            envoyerUnicast(message, adrIpServeur);
        }

        /// <summary>
        /// Préparation des données à envoyer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ajoutScoreBasket_Click(object sender, EventArgs e)
        {
            sportBasket.MajMatch(
                    Convert.ToInt32(txt_bmIdBasket.Text), 
                    Convert.ToInt32(txt_scABasket.Text), 
                    Convert.ToInt32(txt_scBBasket.Text), 
                    Convert.ToInt32(txt_idArbitre.Text)
                    );

            Comm message = new Comm(adrIpLocale, cleUtilisateur, "Aj", sportBasket, "Basket");
            envoyerUnicast(message, adrIpServeur);
        }
        #endregion
        #region Tennis
        /// <summary>
        /// Liste des catégories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_catTennis_Click(object sender, EventArgs e)
        {
            Comm message = new Comm(adrIpLocale, cleUtilisateur, "Cat", "Tennis");
            envoyerUnicast(message, adrIpServeur);
        }

        /// <summary>
        /// Liste des tournois
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_tournoisTennis_Click(object sender, EventArgs e)
        {
            sportTennis.AfficheTournois(Convert.ToInt32(txt_catTennis.Text));

            Comm message = new Comm(adrIpLocale, cleUtilisateur, "T", sportTennis, "Tennis");
            envoyerUnicast(message, adrIpServeur);
        }

        /// <summary>
        /// Liste des matchs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_matchsTennis_Click(object sender, EventArgs e)
        {
            sportTennis.AfficheMatchs(Convert.ToInt32(txt_tournoiTennis.Text));

            Comm message = new Comm(adrIpLocale, cleUtilisateur, "M", sportTennis, "Tennis");
            envoyerUnicast(message, adrIpServeur);
        }
        #endregion

        #endregion

        #region Connexion / déconnexion

        #region Basket
        /// <summary>
        /// Connexion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_connexionBasket_Click(object sender, EventArgs e)
        {
            ConnexionServeur(txt_idUtilisateurBasket.Text, txt_mdpBasket.Text, "Basket");
        }

        /// <summary>
        /// Deconnexion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_deconnexionBasket_Click(object sender, EventArgs e)
        {
            DemandeDecoServ(txt_idUtilisateurBasket.Text, txt_mdpBasket.Text, "Basket");
        }
        #endregion
        #region Tennis

        /// <summary>
        /// Connexion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_connexionTennis_Click(object sender, EventArgs e)
        {
            ConnexionServeur(txt_idUtilisateurTennis.Text, txt_mdpTennis.Text, "Tennis");
        }

        /// <summary>
        /// Déconnexion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_deconnexionTennis_Click(object sender, EventArgs e)
        {
            DemandeDecoServ(txt_idUtilisateurTennis.Text, txt_mdpTennis.Text, "Tennis");
        }
        #endregion

        #endregion

        // Les boutons ne s'activent que si les champs sont remplis
        #region Contrôle de la saisie

        #region Basket
        private void txt_catBasket_TextChanged(object sender, EventArgs e)
        {
            txt_idcatBasket.Text = txt_catBasket.Text;
            txt_idCatScBasket.Text = txt_catBasket.Text;

            // on active le bouton uniquement si le champ est rempli
            if (txt_catBasket.TextLength == 0)
            {
                btn_journBasket.Enabled = false;
            }
            else
            {
                btn_journBasket.Enabled = true;
            }
        }

        private void txt_journBasket_TextChanged(object sender, EventArgs e)
        {
            txt_idJournBasket.Text = txt_journBasket.Text;
            controleBtnMatchsBasket();
        }

        private void txt_idcatBasket_TextChanged(object sender, EventArgs e)
        {
            controleBtnScoresBasket();
            controleBtnMatchsBasket();
        }


        /// <summary>
        /// Contrôle des champs pour l'envoi des scores
        /// </summary>
        private void controleBtnScoresBasket(object sender = null, EventArgs e = null)
        {
            // on active le bouton uniquement si le champ est rempli
            if (
                (txt_idCatScBasket.TextLength == 0) ||
                (txt_idJournBasket.TextLength == 0) ||
                (txt_bmIdBasket.TextLength == 0) ||
                (txt_scABasket.TextLength == 0) ||
                (txt_scBBasket.TextLength == 0) ||
                (txt_idArbitre.TextLength == 0)
                )
            {
                btn_ajoutScoresBasket.Enabled = false;
            }
            else
            {
                btn_ajoutScoresBasket.Enabled = true;
            }
        }

        /// <summary>
        /// Contrôle des champs pour la liste des matchs de basket
        /// </summary>
        private void controleBtnMatchsBasket()
        {
            // on active le bouton uniquement si le champ est rempli
            if ((txt_idcatBasket.TextLength == 0) || (txt_journBasket.TextLength == 0))
            {
                btn_matchsBasket.Enabled = false;
            }
            else
            {
                btn_matchsBasket.Enabled = true;
            }
        }

        #endregion
        #region Tennis

        /// <summary>
        /// Ajout des scores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ajoutScoresTennis_Click(object sender, EventArgs e)
        {
            // la variable peut prendre une valeur entière ET null
            int? sc3_J1 = null;
            int? sc3_J2 = null;

            if (
                (txt_sc3_j1Tennis.TextLength != 0) &&
                (txt_sc3_j2Tennis.TextLength != 0)
                )
            {
                sc3_J1 = Convert.ToInt32(txt_sc3_j1Tennis.Text);
                sc3_J2 = Convert.ToInt32(txt_sc3_j2Tennis.Text);
            }

            sportTennis.MajScores(
                Convert.ToInt32(txt_idMatchTennis.Text),
                Convert.ToInt32(txt_idJoueur1Tennis.Text),
                Convert.ToInt32(txt_sc1_j1Tennis.Text),
                Convert.ToInt32(txt_sc2_j1Tennis.Text), 
                sc3_J1, 
                Convert.ToInt32(txt_idJoueur2Tennis.Text),
                Convert.ToInt32(txt_sc1_j2Tennis.Text),
                Convert.ToInt32(txt_sc2_j2Tennis.Text),
                sc3_J2);

            Comm message = new Comm(adrIpLocale, cleUtilisateur, "Aj", sportTennis, "Tennis");
            envoyerUnicast(message, adrIpServeur);
        }

        // Les boutons ne s'activent que si les champs sont remplis
        #region Contrôle de la saisie

        private void txt_catTennis_TextChanged(object sender, EventArgs e)
        {
            txt_catTennis2.Text = txt_catTennis.Text;

            if (txt_catTennis.TextLength == 0)
            {
                btn_tournoisTennis.Enabled = false;
            }
            else
            {
                btn_tournoisTennis.Enabled = true;
            }
        }

        /// <summary>
        /// Liste des matchs
        /// </summary>
        private void controleBtnMatchsTennis(object sender, EventArgs e)
        {
            if ((txt_tournoiTennis.TextLength == 0) || (txt_catTennis2.TextLength == 0))
            {
                btn_matchsTennis.Enabled = false;
            }
            else
            {
                btn_matchsTennis.Enabled = true;
            }
        }

        /// <summary>
        /// Ajout des scores
        /// </summary>
        private void controleBtnScoresTennis(object sender = null, EventArgs e = null)
        {
            if (
                (txt_idMatchTennis.TextLength == 0) ||
                (txt_idJoueur1Tennis.TextLength == 0) ||
                (txt_sc1_j1Tennis.TextLength == 0) ||
                (txt_sc2_j1Tennis.TextLength == 0) ||
                (txt_idJoueur2Tennis.TextLength == 0) ||
                (txt_sc1_j2Tennis.TextLength == 0) ||
                (txt_sc2_j2Tennis.TextLength == 0)
                )
            {
                btn_ajoutScoresTennis.Enabled = false;
            }
            else
            {
                // les deux champs du 3e set sont soient vides, soient remplis
                if (
                        (
                            (txt_sc3_j1Tennis.TextLength == 0) &&
                            (txt_sc3_j2Tennis.TextLength == 0)
                            ) ||
                        (
                            (txt_sc3_j1Tennis.TextLength > 0) &&
                            (txt_sc3_j2Tennis.TextLength > 0)
                        )
                    )
                {
                    btn_ajoutScoresTennis.Enabled = true;
                }
                else
                {
                    btn_ajoutScoresTennis.Enabled = false;
                }
            }
        }
        #endregion

        #endregion

        #endregion

        #region Méthodes communes

        /// <summary>
        /// Traitement des données à afficher à l'utilisateur dans la boite
        /// </summary>
        /// <param name="msg"></param>
        private void AjoutEtat(string msg)
        {
            rtb_resultats.ResetText();
            rtb_resultats.AppendText(DateTime.Now.ToString("HH:mm:ss") + " : " + msg);
        }

        /// <summary>
        /// Chrono remplaçant l'exception TimeOut du socket - pas fonctionnel en async
        /// </summary>
        /// <param name="time"></param>
        private void NouveauTimer(int time = 5000)
        {
            socketTimeOut = new Timer();
            socketTimeOut.Interval = time;
            socketTimeOut.Tick += new EventHandler(ArretSocket);
            socketTimeOut.Start();
        }

        /// <summary>
        /// Stoppe la connexion et informe l'utilisateur
        /// </summary>
        /// <param name="myObject"></param>
        /// <param name="myEventArgs"></param>
        private void ArretSocket(Object myObject, EventArgs myEventArgs)
        {
            AjoutEtat("La connexion avec le serveur a été interrompue (aucune réponse).");
            ArretTimer();

            DecoServeur();
        }

        /// <summary>
        /// Arrête le chrono
        /// </summary>
        private void ArretTimer()
        {
            socketTimeOut.Tick -= new EventHandler(ArretSocket);
            socketTimeOut.Stop();
        }

        /// <summary>
        /// Gestion du formulaire selon le sport
        /// </summary>
        private void GestionSport(ref List<Control> listeSport)
        {
            foreach (Control ctrl in listeSport)
            {
                // activation ou non de l'élément
                if (ctrl.Enabled == false)
                {
                    ctrl.Enabled = true;
                }
                else
                {
                    ctrl.Enabled = false;
                }

                // on efface pas le contenu si le champ est le nom utilisateur ou le mot de passe
                if (
                    (ctrl.Name != "txt_idUtilisateurBasket") &&
                    (ctrl.Name != "txt_mdpBasket") &&
                    (ctrl.Name != "txt_idUtilisateurTennis") &&
                    (ctrl.Name != "txt_mdpTennis")
                    )
                {
                    // on efface le texte si possible
                    if (ctrl.GetType().Name == "TextBox")
                    {
                        ((TextBox)ctrl).Clear();
                    }
                    else if (ctrl.GetType().Name == "Label")
                    {
                        ((Label)ctrl).ResetText();
                    }
                }
            }
        }

        /// <summary>
        /// Vérifie la présence de l'identifiant, du mot de passe et de l'adresse IP du serveur
        /// </summary>
        /// <param name="utilisateur"></param>
        /// <param name="motDePasse"></param>
        /// <returns></returns>
        private bool ControlePreRequis(string utilisateur, string motDePasse)
        {
            bool estCorrect = true;

            if ((utilisateur.Length == 0) || (motDePasse.Length == 0))
            {
                MessageBox.Show("Aucun identifiant ou mot de passe utilisateur saisi.", "!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                estCorrect = false;
            }

            return estCorrect;
        }

        /// <summary>
        /// Déplace le curseur au prochain bloc de saisie IP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ipServ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Decimal)
            {
                txt_ipServ.SelectionStart = UtilIP.CurseurBlocIP(txt_ipServ.SelectionStart);
            }
        }

        /// <summary>
        /// Vérifie l'IP et connecte
        /// </summary>
        private void ConnexionServeur(string utilisateur, string mdp, string sport)
        {
            if (ControlePreRequis(utilisateur, sport))
            {
                btn_connexionBasket.Enabled = false;
                btn_connexionTennis.Enabled = false;

                // supprime les espaces dans l'IP et regarde si elle est correcte
                if (IPAddress.TryParse((txt_ipServ.Text).Replace(" ", ""), out adrIpServeur))
                {
                    adrIpLocale = IPAddress.Parse(cb_listeInterfaces.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("Vous n'avez pas indiqué l'adresse IP du serveur.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                AjoutEtat("Tentative de connexion ...");

                initReception();
                Comm message = new Comm(adrIpLocale, cleUtilisateur, "Connexion", utilisateur, mdp, sport);
                envoyerUnicast(message, adrIpServeur);
            }
        }

        /// <summary>
        /// Demande au serveur d'être déconnecté
        /// </summary>
        /// <param name="utilisateur"></param>
        /// <param name="mdp"></param>
        /// <param name="sport"></param>
        private void DemandeDecoServ(string utilisateur = "", string mdp = "", string sport = "")
        {
            // cas où on doit arrêter la connexion alors que le visiteur est pas connecté (serveur introuvable, par exemple)
            if (
                (utilisateur != "") ||
                (mdp != "") ||
                (sport != "")
                )
            {
                Comm message = new Comm(adrIpLocale, cleUtilisateur, "Deconnexion", utilisateur, mdp, sport);
                envoyerUnicast(message, adrIpServeur);
            }
        }

        /// <summary>
        /// Déconnecte du serveur
        /// </summary>
        private void DecoServeur()
        {
            finConnexion = true;
            sockReception.Shutdown(SocketShutdown.Send);
            sockReception.Close();

            if (btn_deconnexionBasket.Enabled)
            {
                GestionSport(ref ctrlBasket);
            }
            else if (btn_deconnexionTennis.Enabled)
            {
                GestionSport(ref ctrlTennis);
            }

            btn_connexionBasket.Enabled = true;
            btn_connexionTennis.Enabled = true;

            cleUtilisateur = "-1";
            lbl_idUtilisateur.Text = null;
        }

        #endregion
    }
}
