﻿namespace prototype
{
    partial class Prototype
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Prototype));
            this.btn_catBasket = new System.Windows.Forms.Button();
            this.btn_journBasket = new System.Windows.Forms.Button();
            this.txt_catBasket = new System.Windows.Forms.TextBox();
            this.txt_journBasket = new System.Windows.Forms.TextBox();
            this.btn_matchsBasket = new System.Windows.Forms.Button();
            this.txt_idCatScBasket = new System.Windows.Forms.TextBox();
            this.btn_ajoutScoresBasket = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_idJournBasket = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_bmIdBasket = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_scABasket = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_scBBasket = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_idUtilisateurBasket = new System.Windows.Forms.TextBox();
            this.btn_connexionBasket = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_mdpBasket = new System.Windows.Forms.TextBox();
            this.txt_idcatBasket = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btn_deconnexionBasket = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_listeArbitres = new System.Windows.Forms.Button();
            this.txt_idArbitre = new System.Windows.Forms.TextBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_basket = new System.Windows.Forms.TabPage();
            this.tab_tennis = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_idJoueur2Tennis = new System.Windows.Forms.TextBox();
            this.txt_sc1_j2Tennis = new System.Windows.Forms.TextBox();
            this.txt_sc3_j2Tennis = new System.Windows.Forms.TextBox();
            this.txt_sc2_j2Tennis = new System.Windows.Forms.TextBox();
            this.btn_catTennis = new System.Windows.Forms.Button();
            this.btn_tournoisTennis = new System.Windows.Forms.Button();
            this.txt_catTennis = new System.Windows.Forms.TextBox();
            this.btn_deconnexionTennis = new System.Windows.Forms.Button();
            this.btn_matchsTennis = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_tournoiTennis = new System.Windows.Forms.TextBox();
            this.txt_catTennis2 = new System.Windows.Forms.TextBox();
            this.btn_ajoutScoresTennis = new System.Windows.Forms.Button();
            this.txt_idMatchTennis = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_mdpTennis = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_idJoueur1Tennis = new System.Windows.Forms.TextBox();
            this.txt_idUtilisateurTennis = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.btn_connexionTennis = new System.Windows.Forms.Button();
            this.txt_sc1_j1Tennis = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txt_sc3_j1Tennis = new System.Windows.Forms.TextBox();
            this.txt_sc2_j1Tennis = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rtb_resultats = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbl_idUtilisateur = new System.Windows.Forms.Label();
            this.txt_ipServ = new System.Windows.Forms.MaskedTextBox();
            this.cb_listeInterfaces = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tab_basket.SuspendLayout();
            this.tab_tennis.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_catBasket
            // 
            this.btn_catBasket.Enabled = false;
            this.btn_catBasket.Location = new System.Drawing.Point(217, 8);
            this.btn_catBasket.Name = "btn_catBasket";
            this.btn_catBasket.Size = new System.Drawing.Size(144, 23);
            this.btn_catBasket.TabIndex = 1;
            this.btn_catBasket.Text = "Catégories";
            this.btn_catBasket.UseVisualStyleBackColor = true;
            this.btn_catBasket.Click += new System.EventHandler(this.btn_catBasket_Click);
            // 
            // btn_journBasket
            // 
            this.btn_journBasket.Enabled = false;
            this.btn_journBasket.Location = new System.Drawing.Point(217, 37);
            this.btn_journBasket.Name = "btn_journBasket";
            this.btn_journBasket.Size = new System.Drawing.Size(144, 23);
            this.btn_journBasket.TabIndex = 3;
            this.btn_journBasket.Text = "Journées";
            this.btn_journBasket.UseVisualStyleBackColor = true;
            this.btn_journBasket.Click += new System.EventHandler(this.btn_journBasket_Click);
            // 
            // txt_catBasket
            // 
            this.txt_catBasket.Enabled = false;
            this.txt_catBasket.Location = new System.Drawing.Point(95, 39);
            this.txt_catBasket.Name = "txt_catBasket";
            this.txt_catBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_catBasket.TabIndex = 2;
            this.txt_catBasket.TextChanged += new System.EventHandler(this.txt_catBasket_TextChanged);
            // 
            // txt_journBasket
            // 
            this.txt_journBasket.Enabled = false;
            this.txt_journBasket.Location = new System.Drawing.Point(95, 68);
            this.txt_journBasket.Name = "txt_journBasket";
            this.txt_journBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_journBasket.TabIndex = 4;
            this.txt_journBasket.TextChanged += new System.EventHandler(this.txt_journBasket_TextChanged);
            // 
            // btn_matchsBasket
            // 
            this.btn_matchsBasket.Enabled = false;
            this.btn_matchsBasket.Location = new System.Drawing.Point(217, 66);
            this.btn_matchsBasket.Name = "btn_matchsBasket";
            this.btn_matchsBasket.Size = new System.Drawing.Size(144, 23);
            this.btn_matchsBasket.TabIndex = 5;
            this.btn_matchsBasket.Text = "Matchs";
            this.btn_matchsBasket.UseVisualStyleBackColor = true;
            this.btn_matchsBasket.Click += new System.EventHandler(this.btn_matchsBasket_Click);
            // 
            // txt_idCatScBasket
            // 
            this.txt_idCatScBasket.Location = new System.Drawing.Point(95, 129);
            this.txt_idCatScBasket.Name = "txt_idCatScBasket";
            this.txt_idCatScBasket.ReadOnly = true;
            this.txt_idCatScBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_idCatScBasket.TabIndex = 999999;
            // 
            // btn_ajoutScoresBasket
            // 
            this.btn_ajoutScoresBasket.Enabled = false;
            this.btn_ajoutScoresBasket.Location = new System.Drawing.Point(217, 127);
            this.btn_ajoutScoresBasket.Name = "btn_ajoutScoresBasket";
            this.btn_ajoutScoresBasket.Size = new System.Drawing.Size(144, 23);
            this.btn_ajoutScoresBasket.TabIndex = 11;
            this.btn_ajoutScoresBasket.Text = "Ajout scores";
            this.btn_ajoutScoresBasket.UseVisualStyleBackColor = true;
            this.btn_ajoutScoresBasket.Click += new System.EventHandler(this.btn_ajoutScoreBasket_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "n° catégorie";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "n° journée";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "idCat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "idJournee";
            // 
            // txt_idJournBasket
            // 
            this.txt_idJournBasket.Location = new System.Drawing.Point(95, 155);
            this.txt_idJournBasket.Name = "txt_idJournBasket";
            this.txt_idJournBasket.ReadOnly = true;
            this.txt_idJournBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_idJournBasket.TabIndex = 999999;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "bmId";
            // 
            // txt_bmIdBasket
            // 
            this.txt_bmIdBasket.Enabled = false;
            this.txt_bmIdBasket.Location = new System.Drawing.Point(95, 181);
            this.txt_bmIdBasket.Name = "txt_bmIdBasket";
            this.txt_bmIdBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_bmIdBasket.TabIndex = 6;
            this.txt_bmIdBasket.TextChanged += new System.EventHandler(this.controleBtnScoresBasket);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "score A";
            // 
            // txt_scABasket
            // 
            this.txt_scABasket.Enabled = false;
            this.txt_scABasket.Location = new System.Drawing.Point(95, 207);
            this.txt_scABasket.Name = "txt_scABasket";
            this.txt_scABasket.Size = new System.Drawing.Size(116, 20);
            this.txt_scABasket.TabIndex = 7;
            this.txt_scABasket.TextChanged += new System.EventHandler(this.controleBtnScoresBasket);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "score B";
            // 
            // txt_scBBasket
            // 
            this.txt_scBBasket.Enabled = false;
            this.txt_scBBasket.Location = new System.Drawing.Point(95, 233);
            this.txt_scBBasket.Name = "txt_scBBasket";
            this.txt_scBBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_scBBasket.TabIndex = 8;
            this.txt_scBBasket.TextChanged += new System.EventHandler(this.controleBtnScoresBasket);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "ID";
            // 
            // txt_idUtilisateurBasket
            // 
            this.txt_idUtilisateurBasket.Location = new System.Drawing.Point(95, 257);
            this.txt_idUtilisateurBasket.Name = "txt_idUtilisateurBasket";
            this.txt_idUtilisateurBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_idUtilisateurBasket.TabIndex = 19;
            this.txt_idUtilisateurBasket.Text = "utBasket";
            // 
            // btn_connexionBasket
            // 
            this.btn_connexionBasket.Location = new System.Drawing.Point(14, 309);
            this.btn_connexionBasket.Name = "btn_connexionBasket";
            this.btn_connexionBasket.Size = new System.Drawing.Size(144, 23);
            this.btn_connexionBasket.TabIndex = 18;
            this.btn_connexionBasket.Text = "Connexion";
            this.btn_connexionBasket.UseVisualStyleBackColor = true;
            this.btn_connexionBasket.Click += new System.EventHandler(this.btn_connexionBasket_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 286);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "MDP";
            // 
            // txt_mdpBasket
            // 
            this.txt_mdpBasket.Location = new System.Drawing.Point(95, 283);
            this.txt_mdpBasket.Name = "txt_mdpBasket";
            this.txt_mdpBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_mdpBasket.TabIndex = 21;
            this.txt_mdpBasket.Text = "test";
            // 
            // txt_idcatBasket
            // 
            this.txt_idcatBasket.Location = new System.Drawing.Point(95, 94);
            this.txt_idcatBasket.Name = "txt_idcatBasket";
            this.txt_idcatBasket.ReadOnly = true;
            this.txt_idcatBasket.Size = new System.Drawing.Size(116, 20);
            this.txt_idcatBasket.TabIndex = 999999;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "n° catégorie";
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 9;
            this.lineShape3.X2 = 365;
            this.lineShape3.Y1 = 119;
            this.lineShape3.Y2 = 119;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 9;
            this.lineShape2.X2 = 365;
            this.lineShape2.Y1 = 254;
            this.lineShape2.Y2 = 254;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 9;
            this.lineShape1.X2 = 365;
            this.lineShape1.Y1 = 62;
            this.lineShape1.Y2 = 62;
            // 
            // btn_deconnexionBasket
            // 
            this.btn_deconnexionBasket.Enabled = false;
            this.btn_deconnexionBasket.Location = new System.Drawing.Point(223, 309);
            this.btn_deconnexionBasket.Name = "btn_deconnexionBasket";
            this.btn_deconnexionBasket.Size = new System.Drawing.Size(144, 23);
            this.btn_deconnexionBasket.TabIndex = 28;
            this.btn_deconnexionBasket.Text = "Déconnexion";
            this.btn_deconnexionBasket.UseVisualStyleBackColor = true;
            this.btn_deconnexionBasket.Click += new System.EventHandler(this.btn_deconnexionBasket_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(222, 448);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "IP serveur";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_listeArbitres);
            this.panel1.Controls.Add(this.txt_idArbitre);
            this.panel1.Controls.Add(this.btn_catBasket);
            this.panel1.Controls.Add(this.btn_journBasket);
            this.panel1.Controls.Add(this.txt_catBasket);
            this.panel1.Controls.Add(this.btn_deconnexionBasket);
            this.panel1.Controls.Add(this.btn_matchsBasket);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txt_journBasket);
            this.panel1.Controls.Add(this.txt_idcatBasket);
            this.panel1.Controls.Add(this.btn_ajoutScoresBasket);
            this.panel1.Controls.Add(this.txt_idCatScBasket);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txt_mdpBasket);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txt_idJournBasket);
            this.panel1.Controls.Add(this.txt_idUtilisateurBasket);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btn_connexionBasket);
            this.panel1.Controls.Add(this.txt_bmIdBasket);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txt_scBBasket);
            this.panel1.Controls.Add(this.txt_scABasket);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.shapeContainer2);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(374, 339);
            this.panel1.TabIndex = 32;
            // 
            // btn_listeArbitres
            // 
            this.btn_listeArbitres.Enabled = false;
            this.btn_listeArbitres.Location = new System.Drawing.Point(255, 179);
            this.btn_listeArbitres.Name = "btn_listeArbitres";
            this.btn_listeArbitres.Size = new System.Drawing.Size(106, 23);
            this.btn_listeArbitres.TabIndex = 9;
            this.btn_listeArbitres.Text = "Liste des arbitres";
            this.btn_listeArbitres.UseVisualStyleBackColor = true;
            this.btn_listeArbitres.Click += new System.EventHandler(this.btn_listeArbitres_Click);
            // 
            // txt_idArbitre
            // 
            this.txt_idArbitre.Enabled = false;
            this.txt_idArbitre.Location = new System.Drawing.Point(255, 207);
            this.txt_idArbitre.Name = "txt_idArbitre";
            this.txt_idArbitre.Size = new System.Drawing.Size(106, 20);
            this.txt_idArbitre.TabIndex = 10;
            this.txt_idArbitre.TextChanged += new System.EventHandler(this.controleBtnScoresBasket);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(374, 339);
            this.shapeContainer2.TabIndex = 32;
            this.shapeContainer2.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_basket);
            this.tabControl1.Controls.Add(this.tab_tennis);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(396, 399);
            this.tabControl1.TabIndex = 33;
            // 
            // tab_basket
            // 
            this.tab_basket.Controls.Add(this.panel1);
            this.tab_basket.Location = new System.Drawing.Point(4, 22);
            this.tab_basket.Name = "tab_basket";
            this.tab_basket.Padding = new System.Windows.Forms.Padding(3);
            this.tab_basket.Size = new System.Drawing.Size(388, 373);
            this.tab_basket.TabIndex = 0;
            this.tab_basket.Text = "Basket";
            this.tab_basket.UseVisualStyleBackColor = true;
            // 
            // tab_tennis
            // 
            this.tab_tennis.Controls.Add(this.panel2);
            this.tab_tennis.Location = new System.Drawing.Point(4, 22);
            this.tab_tennis.Name = "tab_tennis";
            this.tab_tennis.Padding = new System.Windows.Forms.Padding(3);
            this.tab_tennis.Size = new System.Drawing.Size(388, 373);
            this.tab_tennis.TabIndex = 1;
            this.tab_tennis.Text = "Tennis";
            this.tab_tennis.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.txt_idJoueur2Tennis);
            this.panel2.Controls.Add(this.txt_sc1_j2Tennis);
            this.panel2.Controls.Add(this.txt_sc3_j2Tennis);
            this.panel2.Controls.Add(this.txt_sc2_j2Tennis);
            this.panel2.Controls.Add(this.btn_catTennis);
            this.panel2.Controls.Add(this.btn_tournoisTennis);
            this.panel2.Controls.Add(this.txt_catTennis);
            this.panel2.Controls.Add(this.btn_deconnexionTennis);
            this.panel2.Controls.Add(this.btn_matchsTennis);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.txt_tournoiTennis);
            this.panel2.Controls.Add(this.txt_catTennis2);
            this.panel2.Controls.Add(this.btn_ajoutScoresTennis);
            this.panel2.Controls.Add(this.txt_idMatchTennis);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.txt_mdpTennis);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.txt_idJoueur1Tennis);
            this.panel2.Controls.Add(this.txt_idUtilisateurTennis);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.btn_connexionTennis);
            this.panel2.Controls.Add(this.txt_sc1_j1Tennis);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.txt_sc3_j1Tennis);
            this.panel2.Controls.Add(this.txt_sc2_j1Tennis);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.shapeContainer1);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(374, 361);
            this.panel2.TabIndex = 34;
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(221, 159);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(116, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "Joueur 2";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(95, 159);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Joueur 1";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_idJoueur2Tennis
            // 
            this.txt_idJoueur2Tennis.Enabled = false;
            this.txt_idJoueur2Tennis.Location = new System.Drawing.Point(221, 175);
            this.txt_idJoueur2Tennis.Name = "txt_idJoueur2Tennis";
            this.txt_idJoueur2Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_idJoueur2Tennis.TabIndex = 10;
            this.txt_idJoueur2Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // txt_sc1_j2Tennis
            // 
            this.txt_sc1_j2Tennis.Enabled = false;
            this.txt_sc1_j2Tennis.Location = new System.Drawing.Point(221, 201);
            this.txt_sc1_j2Tennis.Name = "txt_sc1_j2Tennis";
            this.txt_sc1_j2Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_sc1_j2Tennis.TabIndex = 11;
            this.txt_sc1_j2Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // txt_sc3_j2Tennis
            // 
            this.txt_sc3_j2Tennis.Enabled = false;
            this.txt_sc3_j2Tennis.Location = new System.Drawing.Point(221, 253);
            this.txt_sc3_j2Tennis.Name = "txt_sc3_j2Tennis";
            this.txt_sc3_j2Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_sc3_j2Tennis.TabIndex = 13;
            this.txt_sc3_j2Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // txt_sc2_j2Tennis
            // 
            this.txt_sc2_j2Tennis.Enabled = false;
            this.txt_sc2_j2Tennis.Location = new System.Drawing.Point(221, 227);
            this.txt_sc2_j2Tennis.Name = "txt_sc2_j2Tennis";
            this.txt_sc2_j2Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_sc2_j2Tennis.TabIndex = 12;
            this.txt_sc2_j2Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // btn_catTennis
            // 
            this.btn_catTennis.Enabled = false;
            this.btn_catTennis.Location = new System.Drawing.Point(217, 8);
            this.btn_catTennis.Name = "btn_catTennis";
            this.btn_catTennis.Size = new System.Drawing.Size(144, 23);
            this.btn_catTennis.TabIndex = 1;
            this.btn_catTennis.Text = "Catégories";
            this.btn_catTennis.UseVisualStyleBackColor = true;
            this.btn_catTennis.Click += new System.EventHandler(this.btn_catTennis_Click);
            // 
            // btn_tournoisTennis
            // 
            this.btn_tournoisTennis.Enabled = false;
            this.btn_tournoisTennis.Location = new System.Drawing.Point(217, 37);
            this.btn_tournoisTennis.Name = "btn_tournoisTennis";
            this.btn_tournoisTennis.Size = new System.Drawing.Size(144, 23);
            this.btn_tournoisTennis.TabIndex = 2;
            this.btn_tournoisTennis.Text = "Tournois";
            this.btn_tournoisTennis.UseVisualStyleBackColor = true;
            this.btn_tournoisTennis.Click += new System.EventHandler(this.btn_tournoisTennis_Click);
            // 
            // txt_catTennis
            // 
            this.txt_catTennis.Enabled = false;
            this.txt_catTennis.Location = new System.Drawing.Point(95, 39);
            this.txt_catTennis.Name = "txt_catTennis";
            this.txt_catTennis.Size = new System.Drawing.Size(116, 20);
            this.txt_catTennis.TabIndex = 1;
            this.txt_catTennis.TextChanged += new System.EventHandler(this.txt_catTennis_TextChanged);
            // 
            // btn_deconnexionTennis
            // 
            this.btn_deconnexionTennis.Enabled = false;
            this.btn_deconnexionTennis.Location = new System.Drawing.Point(221, 335);
            this.btn_deconnexionTennis.Name = "btn_deconnexionTennis";
            this.btn_deconnexionTennis.Size = new System.Drawing.Size(144, 23);
            this.btn_deconnexionTennis.TabIndex = 28;
            this.btn_deconnexionTennis.Text = "Déconnexion";
            this.btn_deconnexionTennis.UseVisualStyleBackColor = true;
            this.btn_deconnexionTennis.Click += new System.EventHandler(this.btn_deconnexionTennis_Click);
            // 
            // btn_matchsTennis
            // 
            this.btn_matchsTennis.Enabled = false;
            this.btn_matchsTennis.Location = new System.Drawing.Point(217, 66);
            this.btn_matchsTennis.Name = "btn_matchsTennis";
            this.btn_matchsTennis.Size = new System.Drawing.Size(144, 23);
            this.btn_matchsTennis.TabIndex = 4;
            this.btn_matchsTennis.Text = "Matchs";
            this.btn_matchsTennis.UseVisualStyleBackColor = true;
            this.btn_matchsTennis.Click += new System.EventHandler(this.btn_matchsTennis_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "n° catégorie";
            // 
            // txt_tournoiTennis
            // 
            this.txt_tournoiTennis.Enabled = false;
            this.txt_tournoiTennis.Location = new System.Drawing.Point(95, 68);
            this.txt_tournoiTennis.Name = "txt_tournoiTennis";
            this.txt_tournoiTennis.Size = new System.Drawing.Size(116, 20);
            this.txt_tournoiTennis.TabIndex = 3;
            this.txt_tournoiTennis.TextChanged += new System.EventHandler(this.controleBtnMatchsTennis);
            // 
            // txt_catTennis2
            // 
            this.txt_catTennis2.Location = new System.Drawing.Point(95, 94);
            this.txt_catTennis2.Name = "txt_catTennis2";
            this.txt_catTennis2.ReadOnly = true;
            this.txt_catTennis2.Size = new System.Drawing.Size(116, 20);
            this.txt_catTennis2.TabIndex = 25;
            // 
            // btn_ajoutScoresTennis
            // 
            this.btn_ajoutScoresTennis.Enabled = false;
            this.btn_ajoutScoresTennis.Location = new System.Drawing.Point(217, 127);
            this.btn_ajoutScoresTennis.Name = "btn_ajoutScoresTennis";
            this.btn_ajoutScoresTennis.Size = new System.Drawing.Size(144, 23);
            this.btn_ajoutScoresTennis.TabIndex = 14;
            this.btn_ajoutScoresTennis.Text = "Ajout scores";
            this.btn_ajoutScoresTennis.UseVisualStyleBackColor = true;
            this.btn_ajoutScoresTennis.Click += new System.EventHandler(this.btn_ajoutScoresTennis_Click);
            // 
            // txt_idMatchTennis
            // 
            this.txt_idMatchTennis.Enabled = false;
            this.txt_idMatchTennis.Location = new System.Drawing.Point(95, 129);
            this.txt_idMatchTennis.Name = "txt_idMatchTennis";
            this.txt_idMatchTennis.Size = new System.Drawing.Size(116, 20);
            this.txt_idMatchTennis.TabIndex = 5;
            this.txt_idMatchTennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "n° catégorie";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 312);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "MDP";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "n° tournoi";
            // 
            // txt_mdpTennis
            // 
            this.txt_mdpTennis.Location = new System.Drawing.Point(93, 309);
            this.txt_mdpTennis.Name = "txt_mdpTennis";
            this.txt_mdpTennis.Size = new System.Drawing.Size(116, 20);
            this.txt_mdpTennis.TabIndex = 21;
            this.txt_mdpTennis.Text = "test";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 132);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 9;
            this.label19.Text = "idMatch";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 286);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "ID";
            // 
            // txt_idJoueur1Tennis
            // 
            this.txt_idJoueur1Tennis.Enabled = false;
            this.txt_idJoueur1Tennis.Location = new System.Drawing.Point(95, 175);
            this.txt_idJoueur1Tennis.Name = "txt_idJoueur1Tennis";
            this.txt_idJoueur1Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_idJoueur1Tennis.TabIndex = 6;
            this.txt_idJoueur1Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // txt_idUtilisateurTennis
            // 
            this.txt_idUtilisateurTennis.Location = new System.Drawing.Point(93, 283);
            this.txt_idUtilisateurTennis.Name = "txt_idUtilisateurTennis";
            this.txt_idUtilisateurTennis.Size = new System.Drawing.Size(116, 20);
            this.txt_idUtilisateurTennis.TabIndex = 19;
            this.txt_idUtilisateurTennis.Text = "utTennis";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 178);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 13);
            this.label21.TabIndex = 11;
            this.label21.Text = "IdJoueur";
            // 
            // btn_connexionTennis
            // 
            this.btn_connexionTennis.Location = new System.Drawing.Point(12, 335);
            this.btn_connexionTennis.Name = "btn_connexionTennis";
            this.btn_connexionTennis.Size = new System.Drawing.Size(144, 23);
            this.btn_connexionTennis.TabIndex = 18;
            this.btn_connexionTennis.Text = "Connexion";
            this.btn_connexionTennis.UseVisualStyleBackColor = true;
            this.btn_connexionTennis.Click += new System.EventHandler(this.btn_connexionTennis_Click);
            // 
            // txt_sc1_j1Tennis
            // 
            this.txt_sc1_j1Tennis.Enabled = false;
            this.txt_sc1_j1Tennis.Location = new System.Drawing.Point(95, 201);
            this.txt_sc1_j1Tennis.Name = "txt_sc1_j1Tennis";
            this.txt_sc1_j1Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_sc1_j1Tennis.TabIndex = 7;
            this.txt_sc1_j1Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 256);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 13);
            this.label22.TabIndex = 17;
            this.label22.Text = "(Score set 3)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(11, 204);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "Score set 1";
            // 
            // txt_sc3_j1Tennis
            // 
            this.txt_sc3_j1Tennis.Enabled = false;
            this.txt_sc3_j1Tennis.Location = new System.Drawing.Point(95, 253);
            this.txt_sc3_j1Tennis.Name = "txt_sc3_j1Tennis";
            this.txt_sc3_j1Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_sc3_j1Tennis.TabIndex = 9;
            this.txt_sc3_j1Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // txt_sc2_j1Tennis
            // 
            this.txt_sc2_j1Tennis.Enabled = false;
            this.txt_sc2_j1Tennis.Location = new System.Drawing.Point(95, 227);
            this.txt_sc2_j1Tennis.Name = "txt_sc2_j1Tennis";
            this.txt_sc2_j1Tennis.Size = new System.Drawing.Size(116, 20);
            this.txt_sc2_j1Tennis.TabIndex = 8;
            this.txt_sc2_j1Tennis.TextChanged += new System.EventHandler(this.controleBtnScoresTennis);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(11, 230);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "Score set 2";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4,
            this.lineShape5,
            this.lineShape6});
            this.shapeContainer1.Size = new System.Drawing.Size(374, 361);
            this.shapeContainer1.TabIndex = 32;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape3";
            this.lineShape4.X1 = 8;
            this.lineShape4.X2 = 364;
            this.lineShape4.Y1 = 62;
            this.lineShape4.Y2 = 62;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape2";
            this.lineShape5.X1 = 7;
            this.lineShape5.X2 = 363;
            this.lineShape5.Y1 = 278;
            this.lineShape5.Y2 = 278;
            // 
            // lineShape6
            // 
            this.lineShape6.Name = "lineShape1";
            this.lineShape6.X1 = 8;
            this.lineShape6.X2 = 364;
            this.lineShape6.Y1 = 119;
            this.lineShape6.Y2 = 119;
            // 
            // rtb_resultats
            // 
            this.rtb_resultats.Location = new System.Drawing.Point(12, 471);
            this.rtb_resultats.Name = "rtb_resultats";
            this.rtb_resultats.ReadOnly = true;
            this.rtb_resultats.Size = new System.Drawing.Size(392, 235);
            this.rtb_resultats.TabIndex = 36;
            this.rtb_resultats.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 455);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Résultat requête";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 442);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 37;
            this.label13.Text = "ID bdD :";
            // 
            // lbl_idUtilisateur
            // 
            this.lbl_idUtilisateur.AutoSize = true;
            this.lbl_idUtilisateur.Location = new System.Drawing.Point(65, 442);
            this.lbl_idUtilisateur.Name = "lbl_idUtilisateur";
            this.lbl_idUtilisateur.Size = new System.Drawing.Size(18, 13);
            this.lbl_idUtilisateur.TabIndex = 38;
            this.lbl_idUtilisateur.Text = "ID";
            // 
            // txt_ipServ
            // 
            this.txt_ipServ.Location = new System.Drawing.Point(283, 445);
            this.txt_ipServ.Mask = "###.###.###.###";
            this.txt_ipServ.Name = "txt_ipServ";
            this.txt_ipServ.RejectInputOnFirstFailure = true;
            this.txt_ipServ.ResetOnPrompt = false;
            this.txt_ipServ.Size = new System.Drawing.Size(121, 20);
            this.txt_ipServ.TabIndex = 39;
            this.txt_ipServ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ipServ_KeyDown);
            // 
            // cb_listeInterfaces
            // 
            this.cb_listeInterfaces.FormattingEnabled = true;
            this.cb_listeInterfaces.Location = new System.Drawing.Point(283, 418);
            this.cb_listeInterfaces.Name = "cb_listeInterfaces";
            this.cb_listeInterfaces.Size = new System.Drawing.Size(121, 21);
            this.cb_listeInterfaces.TabIndex = 40;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(193, 421);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 13);
            this.label26.TabIndex = 41;
            this.label26.Text = "Interface réseau";
            // 
            // Prototype
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 718);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.cb_listeInterfaces);
            this.Controls.Add(this.txt_ipServ);
            this.Controls.Add(this.lbl_idUtilisateur);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.rtb_resultats);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Prototype";
            this.Text = "Prototype";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tab_basket.ResumeLayout(false);
            this.tab_tennis.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_catBasket;
        private System.Windows.Forms.Button btn_journBasket;
        private System.Windows.Forms.TextBox txt_catBasket;
        private System.Windows.Forms.TextBox txt_journBasket;
        private System.Windows.Forms.Button btn_matchsBasket;
        private System.Windows.Forms.TextBox txt_idCatScBasket;
        private System.Windows.Forms.Button btn_ajoutScoresBasket;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_idJournBasket;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_bmIdBasket;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_scABasket;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_scBBasket;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_idUtilisateurBasket;
        private System.Windows.Forms.Button btn_connexionBasket;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_mdpBasket;
        private System.Windows.Forms.TextBox txt_idcatBasket;
        private System.Windows.Forms.Label label11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Button btn_deconnexionBasket;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_basket;
        private System.Windows.Forms.TabPage tab_tennis;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_catTennis;
        private System.Windows.Forms.Button btn_tournoisTennis;
        private System.Windows.Forms.TextBox txt_catTennis;
        private System.Windows.Forms.Button btn_deconnexionTennis;
        private System.Windows.Forms.Button btn_matchsTennis;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_tournoiTennis;
        private System.Windows.Forms.TextBox txt_catTennis2;
        private System.Windows.Forms.Button btn_ajoutScoresTennis;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txt_mdpTennis;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_idJoueur1Tennis;
        private System.Windows.Forms.TextBox txt_idUtilisateurTennis;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btn_connexionTennis;
        private System.Windows.Forms.TextBox txt_sc1_j1Tennis;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txt_sc3_j1Tennis;
        private System.Windows.Forms.TextBox txt_sc2_j1Tennis;
        private System.Windows.Forms.Label label24;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private System.Windows.Forms.TextBox txt_idMatchTennis;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RichTextBox rtb_resultats;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_idUtilisateur;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_idJoueur2Tennis;
        private System.Windows.Forms.TextBox txt_sc1_j2Tennis;
        private System.Windows.Forms.TextBox txt_sc3_j2Tennis;
        private System.Windows.Forms.TextBox txt_sc2_j2Tennis;
        private System.Windows.Forms.Button btn_listeArbitres;
        private System.Windows.Forms.TextBox txt_idArbitre;
        private System.Windows.Forms.MaskedTextBox txt_ipServ;
        private System.Windows.Forms.ComboBox cb_listeInterfaces;
        private System.Windows.Forms.Label label26;
    }
}

