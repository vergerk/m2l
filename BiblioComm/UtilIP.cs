﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ajouts
using System.Net;
using System.Net.Sockets;

namespace BiblioComm
{
    public class UtilIP
    {
        /// <summary>
        /// Indique les adresses IP des interfaces
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAdrIpLocaleV4()
        {
            List<string> listeIP = new List<string>();

            // obtient le nom d'hôte de l'ordinateur local
            string hote = Dns.GetHostName();

            // résout un nom d'hôte en vue d'obtenir une adresse IP
            IPHostEntry ipLocales = Dns.GetHostEntry(hote);
            foreach (IPAddress ip in ipLocales.AddressList)
            {
                // compare toutes les adresses trouvées avec une compatible IPv4
                // IPv6 : (ip.AddressFamily == AddressFamily.InterNetworkV6)
                // http://msdn.microsoft.com/fr-fr/library/system.net.ipaddress.addressfamily%28v=vs.110%29.aspx

                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    // on renvoi l'adresse IP locale
                    listeIP.Add(ip.ToString());
                }
            }

            if (listeIP.Count == 0)
            {
                return null; // aucune adresse IPv4
            }
            else
            {
                return listeIP;
            }
        }

        /// <summary>
        /// Indique la nouvelle position du curseur dans les champs IP
        /// </summary>
        /// <param name="posInitiale"></param>
        /// <returns></returns>
        public static int CurseurBlocIP(int posInitiale)
        {
            int posFinale = 0;

            if (posInitiale > 12)
            {
                posFinale = 13;
            }
            else if (posInitiale > 8)
            {
                posFinale = 11;
            }
            else if (posInitiale > 4)
            {
                posFinale = 7;
            }
            else if (posInitiale > 0)
            {
                posFinale = 3;
            }

            return posFinale;
        }
    }
}
