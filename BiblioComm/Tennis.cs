﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioComm
{
    [Serializable]
    public class Tennis
    {
        private int categorie;
        private int tournoi;
        private int match;
        
        private int joueur1;
        private int set1J1;
        private int set2J1;
        private int? set3J1;

        private int joueur2;
        private int set1J2;
        private int set2J2;
        private int? set3J2;     

        #region Accesseurs

        public int Joueur1
        {
            get { return joueur1; }
            set { joueur1 = value; }
        }

        public int Joueur2
        {
            get { return joueur2; }
            set { joueur2 = value; }
        }

        public int Set1J1
        {
            get { return set1J1; }
            set { set1J1 = value; }
        }

        public int Set2J1
        {
            get { return set2J1; }
            set { set2J1 = value; }
        }

        public int? Set3J1
        {
            get { return set3J1; }
            set { set3J1 = value; }
        }

        public int Set1J2
        {
            get { return set1J2; }
            set { set1J2 = value; }
        }

        public int Set2J2
        {
            get { return set2J2; }
            set { set2J2 = value; }
        }

        public int? Set3J2
        {
            get { return set3J2; }
            set { set3J2 = value; }
        }

        public int Match
        {
            get { return match; }
            set { match = value; }
        }

        public int Tournoi
        {
            get { return tournoi; }
            set { tournoi = value; }
        }
        
        public int Categorie
        {
            get { return categorie; }
            set { categorie = value; }
        }

        #endregion

        #region Constructeurs

        /// <summary>
        /// constructeur par défaut
        /// </summary>
        public Tennis()
        {
            categorie = -1;
            tournoi = -1;
            match = -1;
            joueur1 = -1;
            joueur2 = -1;
            set1J1 = -1;
            set2J1 = -1;
            set3J1 = -1;
            set1J2 = -1;
            set2J2 = -1;
            set3J2 = -1;
        }

        #endregion

        #region Méthodes

        /// <summary>
        /// affichage des tournois
        /// </summary>
        /// <param name="cat"></param>
        public void AfficheTournois(int cat)
        {
            categorie = cat;
        }

        /// <summary>
        /// affichage des matchs
        /// </summary>
        /// <param name="tour"></param>
        public void AfficheMatchs(int tour)
        {
            tournoi = tour;
        }

        /// <summary>
        /// mise à jour des scores
        /// </summary>
        /// <param name="idMatch"></param>
        /// <param name="joueur1"></param>
        /// <param name="scSet1_J1"></param>
        /// <param name="scSet2_J1"></param>
        /// <param name="scSet3_J1"></param>
        /// <param name="joueur2"></param>
        /// <param name="scSet1_J2"></param>
        /// <param name="scSet2_J2"></param>
        /// <param name="scSet3_J2"></param>
        public void MajScores(int idMatch, int joueur1, int scSet1_J1, int scSet2_J1, int? scSet3_J1, int joueur2, int scSet1_J2, int scSet2_J2, int? scSet3_J2)
        {
            match = idMatch;

            this.joueur1 = joueur1;
            set1J1 = scSet1_J1;
            set2J1 = scSet2_J1;
            set3J1 = scSet3_J1;

            this.joueur2 = joueur2;
            set1J2 = scSet1_J2;
            set2J2 = scSet2_J2;
            set3J2 = scSet3_J2;
        }

        #endregion
    }
}
