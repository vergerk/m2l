﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ajouts
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BiblioComm
{
    [Serializable]
    public class Binaire
    {
        /// <summary>
        /// On transforme l'objet en chaine binaire
        /// </summary>
        /// <param name="p_message"></param>
        /// <returns></returns>
        public static byte[] Serialiser(Comm p_message)
        {
            // ouverture d'un conteneur dans la mémoire
            MemoryStream flux = new MemoryStream();

            // instanciation d'un objet pour la convertion
            BinaryFormatter formateur = new BinaryFormatter();

            // convertion du MessageReseau reçu et stockage dans le conteneur en mémoire
            formateur.Serialize(flux, p_message);

            // on récupère le contenu en mémoire au format binaire, puis stockage dans un tableau byte[]
            byte[] buffer = flux.GetBuffer();

            // fermeture du conteneur
            flux.Close();

            // renvoi du tableau
            return buffer;
        }

        /// <summary>
        /// On récupère une chaine binaire en objet
        /// </summary>
        /// <param name="p_buffer"></param>
        /// <returns></returns>
        public static Comm Deserialiser(byte[] p_buffer)
        {
            // ouverture d'un conteneur dans la mémoire
            MemoryStream flux = new MemoryStream(p_buffer);

            // instanciation d'un objet pour la convertion
            BinaryFormatter formateur = new BinaryFormatter();

            // désérialisation puis stockage de l'objet en mémoire
            object obj = formateur.Deserialize(flux);

            // fermeture du conteneur
            flux.Close();

            // renvoi de l'objet
            return (Comm)obj;
        }
    }
}
