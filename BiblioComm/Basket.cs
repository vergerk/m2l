﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiblioComm
{
    [Serializable]
    public class Basket
    {
        private int categorie;
        private int journee;
        private int scoreA;
        private int scoreB;
        private int match;

        private int arbitre;

        #region Accesseurs

        public int Match
        {
          get { return match; }
          set { match = value; }
        }

        public int ScoreB
        {
            get { return scoreB; }
            set { scoreB = value; }
        }

        public int ScoreA
        {
            get { return scoreA; }
            set { scoreA = value; }
        }

        public int Journee
        {
            get { return journee; }
            set { journee = value; }
        }

        public int Categorie
        {
            get { return categorie; }
            set { categorie = value; }
        }

        public int Arbitre
        {
            get { return arbitre; }
            set { arbitre = value; }
        }

        #endregion

        #region Constructeurs

        /// <summary>
        /// constructeur de base
        /// </summary>
        public Basket()
        {
            categorie = -1;
            journee = -1;
            scoreA = -1;
            scoreB = -1;
            match = -1;
            this.arbitre = -1;
        }

        #endregion

        #region Méthodes

        /// <summary>
        /// affichage des journées
        /// </summary>
        /// <param name="cat"></param>
        public void AjCategories(int cat)
        {
            categorie = cat;
        }

        /// <summary>
        /// affichage des matchs
        /// </summary>
        /// <param name="jour"></param>
        public void AjJournees(int jour)
        {
            journee = jour;
        }

        /// <summary>
        /// mise à jour des matchs
        /// </summary>
        /// <param name="match"></param>
        /// <param name="scA"></param>
        /// <param name="scB"></param>
        /// <param name="arbitre"></param>
        public void MajMatch(int match, int scA, int scB, int arbitre)
        {
            this.match = match;

            scoreA = scA;
            scoreB = scB;

            this.arbitre = arbitre;
        }

        #endregion
    }
}
