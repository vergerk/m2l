﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ajouts
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;

namespace BiblioComm
{
    [Serializable]
    public class Comm
    {
        private IPAddress ipEmetteur;
        private string typeMessage;
        private string param;

        private string cleEmetteur;

        private object objetSport;

        // connexion utilisateur
        private string identifiant;
        private string mdp;
        private bool? infoServeur;

        // résultat requête
        private DataSet resultats;

        // types des messages

        /*
         *  Cat         liste des catégories
         *  J           liste des journées
         *  M           liste des matchs
         *  Aj          ajoute les scores
         *  Connexion   connexion de l'utilisateur
         *  Resultat    résultat de la demande
         *  Deconnexion
         */

        #region Accesseurs

        public string CleEmetteur
        {
            get { return cleEmetteur; }
            set { cleEmetteur = value; }
        }

        public string Param
        {
          get { return param; }
          set { param = value; }
        }

        public object ObjetSport
        {
            get { return objetSport; }
            set { objetSport = value; }
        }

        public IPAddress IpEmetteur
        {
            get { return ipEmetteur; }
            set { ipEmetteur = value; }
        }
   
        public string TypeMessage
        {
            get { return typeMessage; }
            set { typeMessage = value; }
        }

        public string Identifiant
        {
            get { return identifiant; }
            set { identifiant = value; }
        }

        public string Mdp
        {
            get { return mdp; }
            set { mdp = value; }
        }

        public DataSet Resultat
        {
          get { return resultats; }
          set { resultats = value; }
        }

        public bool? InfoServeur
        {
            get { return infoServeur; }
            set { infoServeur = value; }
        }

        #endregion

        #region Constructeur

        /// <summary>
        /// Constructeur de base
        /// </summary>
        /// <param name="p_ipEmetteur"></param>
        /// <param name="cleEmetteur"></param>
        /// <param name="typeMessage"></param>
        /// <param name="param"></param>
        public Comm(IPAddress p_ipEmetteur, string cleEmetteur, string typeMessage, string param)
        {
            ipEmetteur = p_ipEmetteur;
            this.typeMessage = typeMessage;
            this.cleEmetteur = cleEmetteur;
            
            objetSport = null;
            this.param = param;

            identifiant = null;
            mdp = null;
            infoServeur = null;

            resultats = null;
        }

        /// <summary>
        /// Constructeur avec les résultats de la requête
        /// </summary>
        /// <param name="p_ipEmetteur"></param>
        /// <param name="cleEmetteur"></param>
        /// <param name="typeMessage"></param>
        /// <param name="resultats"></param>
        public Comm(IPAddress p_ipEmetteur, string cleEmetteur, string typeMessage, DataSet resultats)
        {
            ipEmetteur = p_ipEmetteur;
            this.typeMessage = typeMessage;
            this.cleEmetteur = cleEmetteur;

            objetSport = null;
            this.param = null;

            identifiant = null;
            mdp = null;
            infoServeur = null;

            this.resultats = resultats;
        }

        /// <summary>
        /// Constructeur avec le résultat booléen de la connexion
        /// </summary>
        /// <param name="p_ipEmetteur"></param>
        /// <param name="cleEmetteur"></param>
        /// <param name="typeMessage"></param>
        /// <param name="etat"></param>
        /// <param name="param"></param>
        public Comm(IPAddress p_ipEmetteur, string cleEmetteur, string typeMessage, bool etat, string param)
        {
            ipEmetteur = p_ipEmetteur;
            this.typeMessage = typeMessage;
            this.cleEmetteur = cleEmetteur;

            objetSport = null;
            this.param = param;

            identifiant = null;
            mdp = null;
            this.infoServeur = etat;

            this.resultats = null;
        }

        /// <summary>
        /// Connexion de l'utilisateur
        /// </summary>
        /// <param name="p_ipEmetteur"></param>
        /// <param name="cleEmetteur"></param>
        /// <param name="typeMessage"></param>
        /// <param name="utilisateur"></param>
        /// <param name="mdp"></param>
        /// <param name="sport"></param>
        public Comm(IPAddress p_ipEmetteur, string cleEmetteur, string typeMessage, string utilisateur, string mdp, string sport)
        {
            ipEmetteur = p_ipEmetteur;
            this.typeMessage = typeMessage;
            this.cleEmetteur = cleEmetteur;

            this.objetSport = null;
            this.param = sport;

            this.identifiant = utilisateur;
            this.mdp = mdp;
            this.infoServeur = null;

            this.resultats = null;
        }

        /// <summary>
        /// Constructeur pour les informations de sport
        /// </summary>
        /// <param name="p_ipEmetteur"></param>
        /// <param name="cleEmetteur"></param>
        /// <param name="typeMessage"></param>
        /// <param name="objetSport"></param>
        /// <param name="sport"></param>
        public Comm(IPAddress p_ipEmetteur, string cleEmetteur, string typeMessage, object objetSport, string sport)
        {
            ipEmetteur = p_ipEmetteur;
            this.typeMessage = typeMessage;
            this.cleEmetteur = cleEmetteur;

            this.objetSport = objetSport;
            this.param = sport;

            this.identifiant = null;
            this.mdp = null;
            this.infoServeur = null;

            this.resultats = null;
        }

        #endregion
    }
}
